<?php
/*-------------------------------------------------------+
| PHP-Fusion Content Management System
| Copyright (C) 2002 - 2011 Nick Jones
| http://www.php-fusion.co.uk/
+--------------------------------------------------------+
| Filename: admin.php
| Author: Patrik Hoffmann (djmetla)
+--------------------------------------------------------+
| This program is released as free software under the
| Affero GPL license. You can redistribute it and/or
| modify it under the terms of this license which you
| can read by viewing the included agpl.txt or online
| at www.gnu.org/licenses/agpl.html. Removal of this
| copyright header is strictly prohibited without
| written permission from the original author(s).
+--------------------------------------------------------*/
require "../../maincore.php";
require_once (THEMES."templates/admin_header.php");
require INFUSIONS."djmCinema/Includes/Core.php";
if (!checkrights("CIN") || !defined("iAUTH") || !isset($_GET['aid']) || $_GET['aid'] != iAUTH) { redirect(BASEDIR."index.php"); }

$check_accept = dbcount("(*)", CIN_VIDEO, "video_status='0'");
$check_report = dbcount("(*)", CIN_REPORT);
opentable("Navigácia");
		echo "<div style='text-align: center;'>
				<a href='".$redir['admin']."index'>".$t[51]."</a>&nbsp;&middot;&nbsp;
				<a href='".$redir['admin']."albums'>".$t[52]."</a>&nbsp;&middot;&nbsp;
				<a href='".$redir['admin']."videos'>".$t[53]."</a> ".($check_accept ? "(<span style='color: red;'>".$check_accept."</span>)":"")." &nbsp;&middot;&nbsp;
				<a href='".$redir['admin']."reports'>".$t[54]."</a> ".($check_report ? "(<span style='color: red;'>".$check_report."</span>)":"")." &nbsp;&middot;&nbsp;
				<a href='".$redir['admin']."tags'>".$t[55]."</a>&nbsp;&middot;&nbsp;
				<a href='".$redir['admin']."bans'>".$t[56]."</a>&nbsp;&middot;&nbsp;
				<a href='".$redir['admin']."settings'>".$t[57]."</a>&nbsp;&middot;&nbsp;
				<a href='".$redir['admin']."admins'>".$t['57a']."</a>&nbsp;&middot;&nbsp;
				<a href='".$redir['admin']."update'>".$t[58]."</a>
				</div>"; 
closetable();

if (isset($_GET['done_update'])) {
echo "<table id='djmCinemaTooltipMessage' border='0' width='100%'>";
 echo "<tr><td style='background: green; color: #ffffff; padding: 10px; font-size: 13px;' align='center'> ".$t[59]." <a href='#' id='djmCinemaCloseTooltipMessage' style='color: #ffffff; opacity: 0.5;'>".$t[69]."</a> </td> </tr>";
echo "</table>"; 
}elseif (isset($_GET['done_delete'])) {
echo "<table id='djmCinemaTooltipMessage' border='0' width='100%'>";
 echo "<tr><td style='background: green; color: #ffffff; padding: 10px; font-size: 13px;' align='center'>  ".$t[60]." <a href='#' id='djmCinemaCloseTooltipMessage' style='color: #ffffff; opacity: 0.5;'>".$t[69]."</a> </td> </tr>";
echo "</table>";
}elseif (isset($_GET['done_accept'])) {
echo "<table id='djmCinemaTooltipMessage' border='0' width='100%'>";
 echo "<tr><td style='background: green; color: #ffffff; padding: 10px; font-size: 13px;' align='center'> ".$t[61]." <a href='#' id='djmCinemaCloseTooltipMessage' style='color: #ffffff; opacity: 0.5;'>".$t[69]."</a> </td> </tr>";
echo "</table>";
}elseif (isset($_GET['done_block'])) {
echo "<table id='djmCinemaTooltipMessage' border='0' width='100%'>";
 echo "<tr><td style='background: green; color: #ffffff; padding: 10px; font-size: 13px;' align='center'> ".$t[62]." <a href='#' id='djmCinemaCloseTooltipMessage' style='color: #ffffff; opacity: 0.5;'>".$t[69]."</a> </td> </tr>";
echo "</table>";
}elseif (isset($_GET['done_unblock'])) {
echo "<table id='djmCinemaTooltipMessage' border='0' width='100%'>";
 echo "<tr><td style='background: green; color: #ffffff; padding: 10px; font-size: 13px;' align='center'> ".$t[63]." <a href='#' id='djmCinemaCloseTooltipMessage' style='color: #ffffff; opacity: 0.5;'>".$t[69]."</a> </td> </tr>";
echo "</table>";
}elseif (isset($_GET['done_create'])) {
echo "<table id='djmCinemaTooltipMessage' border='0' width='100%'>";
 echo "<tr><td style='background: green; color: #ffffff; padding: 10px; font-size: 13px;' align='center'> ".$t[64]." <a href='#' id='djmCinemaCloseTooltipMessage' style='color: #ffffff; opacity: 0.5;'>".$t[69]."</a> </td> </tr>";
echo "</table>"; 
}elseif (isset($_GET['error_blank'])) {
echo "<table id='djmCinemaTooltipMessage' border='0' width='100%'>";
 echo "<tr><td style='background: red; color: #ffffff; padding: 10px; font-size: 13px;' align='center'> ".$t[65]." <a href='#' id='djmCinemaCloseTooltipMessage' style='color: #ffffff; opacity: 0.5;'>".$t[69]."</a> </td> </tr>";
echo "</table>"; 
}elseif (isset($_GET['access_denied'])) {
echo "<table id='djmCinemaTooltipMessage' border='0' width='100%'>";
 echo "<tr><td style='background: red; color: #ffffff; padding: 10px; font-size: 13px;' align='center'> ".$t['65a']." <a href='#' id='djmCinemaCloseTooltipMessage' style='color: #ffffff; opacity: 0.5;'>".$t[69]."</a> </td> </tr>";
echo "</table>"; 
}elseif (isset($_GET['error_exist'])) {
echo "<table id='djmCinemaTooltipMessage' border='0' width='100%'>";
 echo "<tr><td style='background: red; color: #ffffff; padding: 10px; font-size: 13px;' align='center'> ".$t[66]." <a href='#' id='djmCinemaCloseTooltipMessage' style='color: #ffffff; opacity: 0.5;'>".$t[69]."</a> </td> </tr>";
echo "</table>"; 
}elseif ($cinemaUpdate['version'] > $cinemaVersion) {
echo "<table id='djmCinemaTooltipMessage' border='0' width='100%'>";
 echo "<tr><td style='background: red; color: #ffffff; padding: 10px; font-size: 13px;' align='center'> ".$t[67]." ".$cinemaUpdate['name']." ".$t[68]." <a href='#' id='djmCinemaCloseTooltipMessage' style='color: #ffffff; opacity: 0.5;'>".$t[69]."</a> </td> </tr>";
echo "</table>"; 
}


$available = array('index','albums','reports','tags','bans','settings','update','videos','admins');
if(isset($_GET['p']) && in_array($_GET['p'], $available)) {		
	$page = ucfirst($_GET['p']);
	include (INFUSIONS."djmCinema/Includes/Administration/".$page.".php");
} else { include (INFUSIONS."djmCinema/Includes/Administration/Index.php"); }
	
echo DjmCopyright();
require_once (THEMES."templates/footer.php");

?>