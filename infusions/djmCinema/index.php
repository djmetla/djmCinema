<?php
/*-------------------------------------------------------+
| PHP-Fusion Content Management System
| Copyright (C) 2002 - 2011 Nick Jones
| http://www.php-fusion.co.uk/
+--------------------------------------------------------+
| Filename: index.php
| Author: Patrik Hoffmann (djmetla)
+--------------------------------------------------------+
| This program is released as free software under the
| Affero GPL license. You can redistribute it and/or
| modify it under the terms of this license which you
| can read by viewing the included agpl.txt or online
| at www.gnu.org/licenses/agpl.html. Removal of this
| copyright header is strictly prohibited without
| written permission from the original author(s).
+--------------------------------------------------------*/
require "../../maincore.php";
require_once THEMES."templates/header.php";
require INFUSIONS."djmCinema/Includes/Core.php";

	// Lock
	if ($cinema['set_maintenance'] == 1) { redirect($redir['error']."1"); }
	 
	// Ban
	if (iMEMBER) {
		$ban_sql = dbquery("SELECT * FROM ".CIN_BAN." WHERE ban_user='".$userdata['user_id']."' ");
		$ban_data = dbarray($ban_sql);	
		if (dbrows($ban_sql)) {
			if ($ban_data['ban_expiration'] == 0) redirect($redir['ban'].$ban_data['ban_id']);
			$expiracia = $ban_data['ban_date'] + $ban_data['ban_expiration'];
			if ($expiracia > time()) {
				redirect($redir['ban'].$ban_data['ban_id']);
			}
		}
	}	
		
	// Menu 
	opentable($t[3]);
		if (iMEMBER) {
		echo "<div style='text-align: center;'>
				<a href='".$redir['home']."'>".$t[4]."</a>&nbsp;&middot;&nbsp;
				<a href='".$redir['page']."albums'>".$t[5]."</a>&nbsp;&middot;&nbsp;
				<a href='".$redir['page']."videos'>".$t[6]."</a>&nbsp;&middot;&nbsp;
				".($cinema['set_allow_favorites'] ? "<a href='".$redir['page']."favorites'>".$t[7] ."</a>&nbsp;&middot;&nbsp;":"")."
				".($cinema['set_allow_private_albums'] ? "<a href='".$redir['page']."myalbums'>".$t[8]."</a>&nbsp;&middot;&nbsp;":"")."
				".($cinema['set_allow_private_albums'] ? "<a href='".$redir['page']."myhistory'>".$t['8a']."</a>&nbsp;&middot;&nbsp;":"")."
				".($cinema['set_allow_add_video'] ? "<a href='".$redir['page']."add'>".$t[9]."</a>":"")."
				</div>"; 
		} else {
		echo "<div style='text-align: center;'>
				<a href='".$redir['home']."'>".$t[4]."</a>&nbsp;&middot;&nbsp;
				<a href='".$redir['page']."albums'>".$t[5]."</a>&nbsp;&middot;&nbsp;
				<a href='".$redir['page']."videos'>".$t[6]."</a>
				</div>"; 
		}
				
		// Search
		echo "<div style='text-align:center; padding: 10px;'>";
			echo "<form name='search' action='index.php' method='get'> <input type='hidden' name='p' value='search'>";
			 echo "<input id='cinemaSearch' type='text' name='q' data-defined='".$t[10]." ".$cinema['set_name']."...' class='textbox' style=' padding: 7px; width: 325px;' /> &nbsp;&nbsp;&nbsp; <input id='cinemaSearchSubmit' style='padding: 7px; display: none;' type='submit' class='button' value='".$t[11]."'/>";
			echo "</form>";
		echo "</div>";	
		
		// Admin
		if (iADMIN && checkrights("CIN")) {
			echo "<div style='text-align:center; padding: 10px;'>";
				echo "<strong>Admin</strong><br/>";
				echo "<a href='".$redir['admin']."'>".$t[12]."</a>&nbsp;&middot;&nbsp;";
				echo "<a href='".$redir['admin']."videos'>".$t[13]." (".dbcount("(*)", CIN_VIDEO, "video_status='0'").")</a>&nbsp;&middot;&nbsp;";
				echo "<a href='".$redir['admin']."'>".$t[14]." (".dbcount("(*)", CIN_REPORT).")</a>";
			echo "</div>";		
		}
	closetable();
	
	// Dones & Errors
	if (isset($_GET['done_update'])) {
	echo "<table id='djmCinemaTooltipMessage' border='0' width='100%'>";
	 echo "<tr><td style='background: green; color: #ffffff; padding: 10px; font-size: 13px;' align='center'> ".$t[15]." <a href='#' id='djmCinemaCloseTooltipMessage' style='color: #ffffff; opacity: 0.5;'>".$t[21]."</a> </td> </tr>";
	echo "</table>"; 	
	}elseif (isset($_GET['report_done'])) {
		echo "<table id='djmCinemaTooltipMessage' border='0' width='100%'>";
		 echo "<tr><td style='background: green; color: #ffffff; padding: 10px; font-size: 13px;' align='center'> ".$t[16]." <a href='#' id='djmCinemaCloseTooltipMessage' style='color: #ffffff; opacity: 0.5;'>".$t[21]."</a> </td> </tr>";
		echo "</table>"; 
	}elseif (isset($_GET['delete_done'])) {
		echo "<table id='djmCinemaTooltipMessage' border='0' width='100%'>";
		 echo "<tr><td style='background: green; color: #ffffff; padding: 10px; font-size: 13px;' align='center'> ".$t[17]." <a href='#' id='djmCinemaCloseTooltipMessage' style='color: #ffffff; opacity: 0.5;'>".$t[21]."</a> </td> </tr>";
		echo "</table>";	
	}elseif (isset($_GET['move_done'])) {
		echo "<table id='djmCinemaTooltipMessage' border='0' width='100%'>";
		 echo "<tr><td style='background: green; color: #ffffff; padding: 10px; font-size: 13px;' align='center'> ".$t[18]." <a href='#' id='djmCinemaCloseTooltipMessage' style='color: #ffffff; opacity: 0.5;'>".$t[21]."</a> </td> </tr>";
		echo "</table>";			
	}elseif (isset($_GET['error_password'])) {
		echo "<table id='djmCinemaTooltipMessage' border='0' width='100%'>";
		 echo "<tr><td style='background: red; color: #ffffff; padding: 10px; font-size: 13px;' align='center'> ".$t[19]." <a href='#' id='djmCinemaCloseTooltipMessage' style='color: #ffffff; opacity: 0.5;'>".$t[21]."</a> </td> </tr>";
		echo "</table>"; 
	}elseif (isset($_GET['no_album_exist'])) {
		echo "<table id='djmCinemaTooltipMessage' border='0' width='100%'>";
		 echo "<tr><td style='background: red; color: #ffffff; padding: 10px; font-size: 13px;' align='center'> ".$t[20]." <a href='#' id='djmCinemaCloseTooltipMessage' style='color: #ffffff; opacity: 0.5;'>".$t[21]."</a> </td> </tr>";
		echo "</table>"; 
	}
	
	
	// Parse content
	$available = array('favorites','myalbums','albums','add','video','report','album','discussion','tags','search','password','ageconfirm','videos','myhistory');
	if(isset($_GET['p']) && in_array($_GET['p'], $available)) {		
		$page = ucfirst($_GET['p']);
		include (INFUSIONS."djmCinema/Includes/Pages/".$page.".php");
	} else { include (INFUSIONS."djmCinema/Includes/Pages/Index.php"); }
	
	
	if (isset($_GET['user']) && !empty($_GET['user'])) {
			redirect($redir['page']."albums&amp;uid=".$_GET['user']);
	}
	
echo DjmCopyright();
require_once THEMES."templates/footer.php";
?>