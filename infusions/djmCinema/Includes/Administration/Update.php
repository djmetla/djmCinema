<?php
/*-------------------------------------------------------+
| PHP-Fusion Content Management System
| Copyright (C) 2002 - 2011 Nick Jones
| http://www.php-fusion.co.uk/
+--------------------------------------------------------+
| Filename: updates.php
| Author: Patrik Hoffmann (djmetla)
+--------------------------------------------------------+
| This program is released as free software under the
| Affero GPL license. You can redistribute it and/or
| modify it under the terms of this license which you
| can read by viewing the included agpl.txt or online
| at www.gnu.org/licenses/agpl.html. Removal of this
| copyright header is strictly prohibited without
| written permission from the original author(s).
+--------------------------------------------------------*/
if (!defined("IN_FUSION")) { die('Access Denied'); }
add_to_title("&nbsp;-&nbsp;".$cinema['set_name']." - ".$t[554]);

opentable($cinema['set_name']." - ".$t[555]." - v.".$cinemaVersion);
	if ($cinemaUpdate['version'] > $cinemaVersion) {
		echo "<div style='text-align:center; width: 100%; padding: 20px; font-size: 14px;'> ".$t[556]." </div>";
		echo "<table border='0' align='center' width='100%' cellpadding='0' cellspacing='1'>";
			echo "<tr>";
			echo "<td class='tbl1' style='width: 50%; text-align: right; padding-right:5px;'> <strong>".$t[557].": </strong> <br><span style='font-size: 9px;'>".$t[558]."</span> </td>";
			echo "<td class='tbl2' style='padding-left: 15px;'> ".$cinemaVersion." </td>";
			echo "</tr>";		
			echo "<tr>";
			echo "<td class='tbl1' style='width: 50%; text-align: right; padding-right:5px;'> <strong>".$t[559].": </strong> <br><span style='font-size: 9px;'>".$t[560]."</span> </td>";
			echo "<td class='tbl2' style='padding-left: 15px;'> ".$cinemaUpdate['version']." </td>";
			echo "</tr>";	
		echo "</table>";
		echo "<br/>";
		echo "<table border='0' align='center' width='100%' cellpadding='0' cellspacing='1'>";
			echo "<tr>";
			echo "<td class='tbl1' style='width: 50%; text-align: right; padding-right:5px;'> <strong>".$t[561].": </strong> <br><span style='font-size: 9px;'>".$t[562]."</span> </td>";
			echo "<td class='tbl2' style='padding-left: 15px;'> ".$cinemaUpdate['date']['date']." </td>";
			echo "</tr>";		
			echo "<tr>";
			echo "<td class='tbl1' style='width: 50%; text-align: right; padding-right:5px;'> <strong>".$t[563].": </strong> <br><span style='font-size: 9px;'>".$t[564]."</span> </td>";
			echo "<td class='tbl2' style='padding-left: 15px;'> ".$cinemaUpdate['name']." </td>";
			echo "</tr>";										
			echo "<tr>";
			echo "<td class='tbl1' style='width: 50%; text-align: right; padding-right:5px;' valign='top'> <strong>".$t[570].": </strong> <br><span style='font-size: 9px;'>".$t[571]."</span> </td>";
			echo "<td class='tbl2' style='padding-left: 15px;'> <a href='".$cinemaUpdate['download']."' target='_blank'>".$cinemaUpdate['name']."</a> </td>";
			echo "</tr>";					
		echo "</table>";
	
	} else {
		echo "<div style='text-align: center; padding: 25px;'><span style='font-size: 14px;'>".$t[572]."</span> <br/>".$t[573]."<br/><br/><span style='font-size: 10px; color: red;'> ".$t[574]." <a href='http://www.djmetla.eu' target='_blank'>www.djmetla.eu</a> </span></div>";
	}
closetable();
if ($cinemaUpdate['version'] > $cinemaVersion) {
opentable($t[565]);
	echo $cinemaUpdate['changelog'];
closetable();
}




?>