<?php
/*-------------------------------------------------------+
| PHP-Fusion Content Management System
| Copyright (C) 2002 - 2011 Nick Jones
| http://www.php-fusion.co.uk/
+--------------------------------------------------------+
| Filename: bans.php
| Author: Patrik Hoffmann (djmetla)
+--------------------------------------------------------+
| This program is released as free software under the
| Affero GPL license. You can redistribute it and/or
| modify it under the terms of this license which you
| can read by viewing the included agpl.txt or online
| at www.gnu.org/licenses/agpl.html. Removal of this
| copyright header is strictly prohibited without
| written permission from the original author(s).
+--------------------------------------------------------*/
if (!defined("IN_FUSION")) { die('Access Denied'); }

add_to_title("&nbsp;-&nbsp;".$cinema['set_name']." - ".$t[375]);


	if (!iSUPERADMIN) {
	$admin_check = dbcount("(*)", CIN_ADMIN, "admin_allow_ban='1' AND admin_user='".$userdata['user_id']."' ");
	if (!$admin_check) { redirect($redir['admin']."index&amp;access_denied"); }
	}


	// Search ban
	if (isset($_GET['q']) && !empty($_GET['q'])) {
		opentable($cinema['set_name']." ".$t[376]);
		echo "<table border='0' align='center' width='100%' cellpadding='0' cellspacing='1'>";
			$sql = dbquery("SELECT * FROM ".DB_PREFIX."users WHERE user_id='".$_GET['q']."' OR user_name LIKE '%".$_GET['q']."%'");
			if (!dbrows($sql)) { echo "<tr><td class='tbl1' align='center' colspan='2'> ".$t[377]." </td></tr>"; }
			while ($data = dbarray($sql)) {
		
				echo "<tr>";
					echo "<td class='tbl1'> <a href='".BASEDIR."profile.php?lookup=".$data['user_id']."'>".$data['user_name']."</a> </td>";
					echo "<td class='tbl2' align='center'> ".$t[378].": ".dbcount("(*)", CIN_VIDEO, "video_user='".$data['user_id']."'").", ".$t[379].": ".dbcount("(*)", CIN_GALLERY, "gallery_user='".$data['user_id']."'")."</td>";
					echo "<td class='tbl1' align='center'> <a href='".$redir['admin']."bans&amp;add=".$data['user_id']."'>".$t[380]."</a>  </td>";

				echo "</tr>";		
			}	
		echo "</table>";		
		closetable();
		
	// Add ban
	}elseif (isset($_GET['add']) && isnum($_GET['add'])) {
		opentable($cinema['set_name']." ".$t[381]);
			echo "<form action='' method='post'>";
				$sql = dbquery("SELECT * FROM ".DB_PREFIX."users WHERE user_id='".$_GET['add']."' ");
				$data = dbarray($sql);
				echo "<table border='0' align='center' width='100%' cellpadding='0' cellspacing='1'>";
					echo "<tr>";
					echo "<td class='tbl1' style='width: 40%; text-align: right; padding-right:5px;'> <strong> ".$t[382].": </strong> </td>";
					echo "<td class='tbl2' style='padding-left: 15px;'> <a href='".BASEDIR."profile.php?lookup=".$_GET['add']."'>".$data['user_name']."</a> <input type='hidden' name='user' value='".$data['user_id']."'/> </td>";
					echo "</tr>";		
					
					echo "<tr>";
					echo "<td class='tbl1' style='width: 40%; text-align: right; padding-right:5px;'> <strong> ".$t[383].": </strong> </td>";
					echo "<td class='tbl2' style='padding-left: 15px;'> <select name='expiration'>
							<option value='300'>".$t[384]."</option>
							<option value='900'>".$t[385]."</option>
							<option value='1800'>".$t[386]."</option>
							<option value='3600'>".$t[387]."</option>
							<option value='86400'>".$t[388]."</option>
							<option value='604800'>".$t[389]."</option>
							<option value='2678400'>".$t[390]."</option>
							<option value='0'>".$t[391]."</option>
					</select></td>";
					echo "</tr>";
					
					echo "<tr>";
					echo "<td class='tbl1' style='width: 40%; text-align: right; padding-right:5px;'> <strong> ".$t[392].": </strong> </td>";
					echo "<td class='tbl2' style='padding-left: 15px;'> <textarea name='reason' style='width: 300px; height: 90px;' maxlength='100'></textarea></td>";
					echo "</tr>";					
		
					echo "<tr>";
					echo "<td class='tbl1' style='width: 40%; text-align: right; padding-right:5px;'> <strong> ".$t[393].": </strong> </td>";
					echo "<td class='tbl2' style='padding: 15px;'>".$t[394].": <br/><br/>  <b>Text:</b> <br/> <i>".$cinema['set_custom_bantext']."</i></td>";
					echo "</tr>";	

				   echo "<tr><td colspan='2' style='padding: 10px;'><input type='submit' value='".$t[395]."' name='add_ban' class='button'></td></tr>";	
				echo "</table>";
			echo "</form>";			
		closetable();
	
		if (isset($_POST['add_ban'])) {
		
			$form = array(
				'user' => $_POST['user'],
				'expiration' => $_POST['expiration'],
				'reason' => (empty($_POST['reason']) ? $cinema['set_custom_bantext']:$_POST['reason'])
			);
			
			$search = dbcount("(*)", CIN_BAN, "ban_user='".$form['user']."'");
			if ($search) {
				dbquery("UPDATE ".CIN_BAN." SET ban_date='".time()."', ban_reason='".$form['reason']."', ban_expiration='".$form['expiration']."', ban_admin='".$userdata['user_id']."' WHERE ban_user='".$form['user']."' ");
			} else {		
				dbquery("INSERT INTO ".CIN_BAN." (ban_user, ban_date, ban_reason, ban_expiration, ban_admin) VALUES ('".$form['user']."','".time()."','".$form['reason']."','".$form['expiration']."','".$userdata['user_id']."')");
			}
			redirect($redir['admin']."bans&amp;done_create");
		}
	
	
	// Delete ban
	}elseif (isset($_GET['delete']) && isnum($_GET['delete'])) {
	
		dbquery("DELETE FROM ".CIN_BAN." WHERE ban_id='".$_GET['delete']."'");
		redirect($redir['admin']."bans&amp;done_delete");
	
	} else {
	
		opentable($cinema['set_name']." ".$t[396]);
		echo "<div style='text-align:center; padding: 10px;'>";
			echo "<form name='search' action='admin.php' method='get'> <input type='hidden' name='aid' value='".iAUTH."' /> <input type='hidden' name='p' value='bans'>";
			 echo "<input id='cinemaSearch' type='text' name='q' data-defined='".$t[397]."' class='textbox' style=' padding: 7px; width: 325px;' /> &nbsp;&nbsp;&nbsp; <input id='cinemaSearchSubmit' style='padding: 7px; display: none;' type='submit' class='button' value='".$t[398]."'/>";
			echo "</form>";
		echo "</div>";
		closetable();
		
		opentable($cinema['set_name']." - ".$t[375]);
		
			echo "<table border='0' align='center' width='100%' cellpadding='0' cellspacing='1'>";
				$sql = dbquery("SELECT t1.*, t2.user_name as user, t3.user_name as admin FROM ".CIN_BAN." as t1 LEFT JOIN ".DB_PREFIX."users as t2 ON t2.user_id=t1.ban_user LEFT JOIN ".DB_PREFIX."users as t3 ON t3.user_id=t1.ban_admin ORDER BY ban_id ASC");
				if (!dbrows($sql)) { echo "<tr><td class='tbl1' align='center' colspan='2'> ".$t[399]." </td></tr>"; }
				while ($data = dbarray($sql)) {
				
					$expiration = $data['ban_date'] + $data['ban_expiration'];
					if ($data['ban_expiration'] != 0) {
						if ($expiration > time()) {
							$status = "<span style='color: red;'>".$t[400]."</span>";
						} else {
							$status = "<span style='color: green;'>".$t[401]."</span>";
						}	
					}else{
						$status = $t[402];
					}	
					
					echo "<tr>";
						echo "<td class='tbl1'> <a href='".BASEDIR."profile.php?lookup=".$data['ban_user']."'>".$data['user']."</a> </td>";
						echo "<td class='tbl2'> ".$t[403] ." ".strftime("%d.%m.%Y ".$t[404]." %H:%M", $data['ban_date'])." ".$t[405]." <a href='".BASEDIR."profile.php?lookup=".$data['ban_admin']."'>".$data['admin']."</a> </td>";
						echo "<td class='tbl1' align='center'> ".$status." (".$t[406].": ".strftime("%d.%m.%Y %H:%M", $expiration).")  </td>";
						echo "<td class='tbl2' align='center'> ".$t[407].": <span title='".$data['ban_reason']."'>".trimlink($data['ban_reason'], 60)."</span> </td>";
						echo "<td class='tbl1' align='center'> <a href='".$redir['admin']."bans&amp;delete=".$data['ban_id']."'>".$t[408]."</a> </td>";
					echo "</tr>";		
				}	
			echo "</table>";	
		
		closetable();
	}


?>