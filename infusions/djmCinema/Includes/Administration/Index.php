<?php
/*-------------------------------------------------------+
| PHP-Fusion Content Management System
| Copyright (C) 2002 - 2011 Nick Jones
| http://www.php-fusion.co.uk/
+--------------------------------------------------------+
| Filename: Index.php
| Author: Patrik Hoffmann (djmetla)
+--------------------------------------------------------+
| This program is released as free software under the
| Affero GPL license. You can redistribute it and/or
| modify it under the terms of this license which you
| can read by viewing the included agpl.txt or online
| at www.gnu.org/licenses/agpl.html. Removal of this
| copyright header is strictly prohibited without
| written permission from the original author(s).
+--------------------------------------------------------*/
if (!defined("IN_FUSION")) { die('Access Denied'); }
add_to_title("&nbsp;-&nbsp;".$cinema['set_name']." - Index");

 $view_sql = dbquery("SELECT * FROM ".CIN_VIDEO." ORDER BY video_viewed DESC LIMIT 1");
 $view_data = dbarray($view_sql);
 
 $viewall_sql = dbquery("SELECT video_viewed FROM ".CIN_VIDEO." ");
 $all_video_view = "";
 while ( $count = dbarray($viewall_sql)) {
	$all_video_view += $count['video_viewed'];
 }

opentable($cinema['set_name']." - ".$t[409]);
echo "<div style='padding: 5px; font-size: 14px;'>";
echo "<table cellpadding='1' cellspacing='1'>";
	echo "<tr> <td class='tbl1' align='right' width='250' style='font-weight: bold;'>".$t[410].":</td> <td class='tbl2' width='300'> ".dbcount("(*)", CIN_VIDEO)." </td> </tr>";
	echo "<tr> <td class='tbl1' align='right' width='250' style='font-weight: bold;'>".$t[411].":</td> <td class='tbl2' width='300'> ".dbcount("(*)", CIN_VIDEO, "video_status='0'")." </td> </tr>";
	echo "<tr> <td class='tbl1' align='right' width='250' style='font-weight: bold;'>".$t[412].":</td> <td class='tbl2' width='300'> ".dbcount("(*)", CIN_GALLERY, "gallery_type='PUBLIC'")." </td> </tr>";
	echo "<tr> <td class='tbl1' align='right' width='250' style='font-weight: bold;'>".$t[413].":</td> <td class='tbl2' width='300'> ".dbcount("(*)", CIN_GALLERY, "gallery_type='USER'")." </td> </tr>";
	echo "<tr> <td class='tbl1' align='right' width='250' style='font-weight: bold;'>".$t[414].":</td> <td class='tbl2' width='300'> ".dbcount("(*)", CIN_REPORT)." </td> </tr>";
	echo "<tr> <td class='tbl1' align='right' width='250' style='font-weight: bold;'>".$t[415].":</td> <td class='tbl2' width='300'> ".dbcount("(*)", CIN_TAGS_VALUES)." </td> </tr>";
	echo "<tr> <td class='tbl1' align='right' width='250' style='font-weight: bold;'>".$t[416].":</td> <td class='tbl2' width='300'> ".dbcount("(*)", CIN_BAN)." </td> </tr>";
	echo "<tr> <td class='tbl1' align='right' width='250' style='font-weight: bold;'>".$t[417].":</td> <td class='tbl2' width='300'> ".($all_video_view != 0 ? number_format($all_video_view):0)."x </td> </tr>";
	echo "<tr> <td class='tbl1' align='right' width='250' style='font-weight: bold;'>".$t[418].":</td> <td class='tbl2' width='300'> <a href='".$redir['page']."video&vid=".$view_data['video_id']."'> ".$view_data['video_name']." </a> </td> </tr>";
	echo "<tr> <td class='tbl1' align='right' width='250' style='font-weight: bold;'>".$t[419].":</td> <td class='tbl2' width='300'> ".dbcount("(*)", CIN_VIDEO, "video_status='2'")." </td> </tr>";
echo "</table>";
echo "</div>";
closetable();

opentable($cinema['set_name']." - ".$t[420]);
echo "<div style='padding: 5px; font-size: 14px;'>";
echo "<table cellpadding='1' cellspacing='1'>";
	echo "<tr> <td class='tbl1' align='right' width='250' style='font-weight: bold;'>Youtube.com:</td> <td class='tbl2' width='300'> ".dbcount("(*)", CIN_VIDEO, "video_source='YOUTUBE'")."  </td> </tr>";
	echo "<tr> <td class='tbl1' align='right' width='250' style='font-weight: bold;'>Vimeo.com:</td> <td class='tbl2' width='300'> ".dbcount("(*)", CIN_VIDEO, "video_source='VIMEO'")." </td> </tr>";
	echo "<tr> <td class='tbl1' align='right' width='250' style='font-weight: bold;'>Dailymotion.com:</td> <td class='tbl2' width='300'> ".dbcount("(*)", CIN_VIDEO, "video_source='DAILYMOTION'")." </td> </tr>";
	echo "<tr> <td class='tbl1' align='right' width='250' style='font-weight: bold;'>Metacafe.com:</td> <td class='tbl2' width='300'> ".dbcount("(*)", CIN_VIDEO, "video_source='METACAFE'")." </td> </tr>";
	echo "<tr> <td class='tbl1' align='right' width='250' style='font-weight: bold;'>Break.com:</td> <td class='tbl2' width='300'> ".dbcount("(*)", CIN_VIDEO, "video_source='BREAK'")." </td> </tr>";
	echo "<tr> <td class='tbl1' align='right' width='250' style='font-weight: bold;' colspan='2'> <strong> ".$t[421]." </strong> </td> </tr>";
	echo "<tr> <td class='tbl1' align='right' width='250' style='font-weight: bold;'>Redtube.com:</td> <td class='tbl2' width='300'> ".dbcount("(*)", CIN_VIDEO, "video_source='REDTUBE'")." </td> </tr>";
	echo "<tr> <td class='tbl1' align='right' width='250' style='font-weight: bold;'>Pornhub.com:</td> <td class='tbl2' width='300'> ".dbcount("(*)", CIN_VIDEO, "video_source='PORNHUB'")." </td> </tr>";
echo "</table>";
echo "</div>";
closetable();
?>