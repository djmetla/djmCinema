<?php
/*-------------------------------------------------------+
| PHP-Fusion Content Management System
| Copyright (C) 2002 - 2011 Nick Jones
| http://www.php-fusion.co.uk/
+--------------------------------------------------------+
| Filename: settings.php
| Author: Patrik Hoffmann (djmetla)
+--------------------------------------------------------+
| This program is released as free software under the
| Affero GPL license. You can redistribute it and/or
| modify it under the terms of this license which you
| can read by viewing the included agpl.txt or online
| at www.gnu.org/licenses/agpl.html. Removal of this
| copyright header is strictly prohibited without
| written permission from the original author(s).
+--------------------------------------------------------*/
if (!defined("IN_FUSION")) { die('Access Denied'); }
add_to_title("&nbsp;-&nbsp;".$cinema['set_name']." - ".$t[439]);

if (!iSUPERADMIN) {
$admin_check = dbcount("(*)", CIN_ADMIN, "admin_allow_settings='1' AND admin_user='".$userdata['user_id']."' ");
if (!$admin_check) { redirect($redir['admin']."index&amp;access_denied"); }
}


// Update server settings
if (isset($_POST['submitserver'])) {
		
	 $form = array(
		'youtube' 			=> (isset($_POST['fyoutube']) ? 1:0),
		'vimeo' 			=> (isset($_POST['fvimeo']) ? 1:0),
		'dailymotion' 	=> (isset($_POST['fdailymotion']) ? 1:0),
		'metacafe' 		=> (isset($_POST['fmetacafe']) ? 1:0),
		'break' 				=> (isset($_POST['fbreak']) ? 1:0),
		'redtube' 			=> (isset($_POST['fredtube']) ? 1:0),
		'pornhub' 			=> (isset($_POST['fpornhub']) ? 1:0)
	 );
	 
	 $update = "
		set_allow_youtube = '".$form['youtube']."',
		set_allow_vimeo = '".$form['vimeo']."',
		set_allow_dailymotion = '".$form['dailymotion']."',
		set_allow_metacafe = '".$form['metacafe']."',
		set_allow_break = '".$form['break']."',
		set_allow_redtube = '".$form['redtube']."',
		set_allow_pornhub = '".$form['pornhub']."'
	 ";
	 
	 dbquery("UPDATE ".CIN_SETTINGS." SET ".$update);
	 redirect($redir['admin']."settings&amp;done_update");
	 unset($form, $update);
}

// Update global Settings
if (isset($_POST['submit'])) {
	
	$form = array(
		'name'  				=> (!empty($_POST['fname']) ? htmlspecialchars($_POST['fname']): "djmCinema"),
		'countvideo'			=> (!empty($_POST['fcountvideo']) && isnum($_POST['fcountvideo']) ? $_POST['fcountvideo'] : "20"),
		'maintenance'		=> (isset($_POST['fmaintenance']) ? 1:0),
		'webalbum'			=> (isset($_POST['fwebalbum'])? 1:0),
		'videolist'				=> ($_POST['fvideolist'] == 'LIST' || $_POST['fvideolist'] == 'TABLE' ? $_POST['fvideolist']:'LIST'),
		'albumlist'			=> ($_POST['falbumlist'] == 'LIST' || $_POST['falbumlist'] == 'TABLE' ? $_POST['falbumlist']:'LIST'),
		'linkstyle'				=> ($_POST['flinkstyle'] == 'SIMPLE' || $_POST['flinkstyle'] == 'FULL' ? $_POST['flinkstyle']:'FULL'),
		'addvideo'			=> (isset($_POST['faddvideo']) ? 1:0),
		'acceptvideo'		=> (isset($_POST['facceptvideo']) ? 1:0),
		'comments'			=> (isset($_POST['fcomments']) ? 1:0),
		'ratings'				=> (isset($_POST['fratings']) ? 1:0),
		'width'				=> (!empty($_POST['fwidth']) && isnum($_POST['fwidth']) ? $_POST['fwidth']:'640'),
		'height'				=> (!empty($_POST['fheight']) && isnum($_POST['fheight']) ? $_POST['fheight']:'480'),
		'customalbum'		=> (isset($_POST['fcustomalbum']) ? 1:0),
		'favorites'			=> (isset($_POST['ffavorites']) ? 1:0),
		'banreason'			=> htmlspecialchars($_POST['fbanreason']),
		'charset'				=> $_POST['fcharset'],
		'translatetype'		=> $_POST['ftranslatetype'],
		'translate'			=> $_POST['ftranslate'],
		'commentslimit'	=> (!empty($_POST['fcountcomments']) && isnum($_POST['fcountcomments']) ? $_POST['fcountcomments'] : "20"), 
		'bordercolor'		=> (empty($_POST['fbordercolor']) ? 'c4c1c1':$_POST['fbordercolor']),
		'videoimg'			=> (empty($_POST['fvideoimg']) ? 'video.png':$_POST['fvideoimg']),
		'albumimg'			=> (empty($_POST['falbumimg']) ? 'album.png':$_POST['falbumimg']),
		'lockimg'				=> (empty($_POST['flockimg']) ? 'locked.png':$_POST['flockimg']),
		'panelcount'			=> (!empty($_POST['fpanelcountvideo']) && isnum($_POST['fpanelcountvideo']) ? $_POST['fpanelcountvideo']:'5'),
		'panelcontent'		=> (!empty($_POST['fpanelcontent']) && isnum($_POST['fpanelcontent']) && $_POST['fpanelcontent'] < 6 ? $_POST['fpanelcontent']:'1'),
		'panelpadding'		=> (!empty($_POST['fpanelpadding']) && isnum($_POST['fpanelpadding']) ? $_POST['fpanelpadding']:'10')
	);
	

	$update = "
		set_name = '".$form['name']."',
		set_maintenance = '".$form['maintenance']."',
		set_allow_comments = '".$form['comments']."',
		set_allow_ratings = '".$form['ratings']."',
		set_allow_favorites = '".$form['favorites']."',
		set_allow_private_albums = '".$form['customalbum']."',
		set_allow_add_video = '".$form['addvideo']."',
		set_allow_admin_accept = '".$form['acceptvideo']."',
		set_custom_bantext = '".$form['banreason']."',
		set_video_width = '".$form['width']."',
		set_video_height = '".$form['height']."',
		set_index_allow_web_album_panel = '".$form['webalbum']."',
		set_index_panel_countgallery = '".$form['countvideo']."',
		set_video_list = '".$form['videolist']."',
		set_album_list = '".$form['albumlist']."',
		set_cinema_link_style = '".$form['linkstyle']."',
		set_charset	= '".$form['charset']."',
		set_translate_type = '".$form['translatetype']."',
		set_translate = '".$form['translate']."',
		set_comments_limit = '".$form['commentslimit']."',
		set_border_color = '".$form['bordercolor']."',
		set_video_img = '".$form['videoimg']."',
		set_album_img = '".$form['albumimg']."',
		set_lock_img = '".$form['lockimg']."',
		set_panel_content_type = '".$form['panelcontent']."',
		set_panel_count_item = '".$form['panelcount']."',
		set_panel_padding = '".$form['panelpadding']."'
	";

	dbquery("UPDATE ".CIN_SETTINGS." SET ".$update);
	redirect($redir['admin']."settings&amp;done_update");
	unset($form, $update);	
}



opentable($cinema['set_name']." - ".$t[440]);
echo "<form action='' method='post'>";
	echo "<table border='0' align='center' width='100%' cellpadding='0' cellspacing='0'>";
		echo "<tr> <td colspan='2' class='forum-caption'> ".THEME_BULLET." ".$t[441].": </td></tr>";
		
		echo "<tr>";
		echo "<td class='tbl1' style='width: 50%; text-align: right; padding-right:5px;'> <strong>Youtube.com</strong> <br><span style='font-size: 9px;'>".$t[442]."</span> </td>";
		echo "<td class='tbl2' style='padding-left: 15px;'> <input type='checkbox' name='fyoutube' ".($cinema['set_allow_youtube'] == 1 ? "checked":"")."/> </td>";
		echo "</tr>";	
		
		echo "<tr>";
		echo "<td class='tbl1' style='width: 50%; text-align: right; padding-right:5px;'> <strong>Vimeo.com</strong> <br><span style='font-size: 9px;'>".$t[443]."</span> </td>";
		echo "<td class='tbl2' style='padding-left: 15px;'> <input type='checkbox' name='fvimeo' ".($cinema['set_allow_vimeo'] == 1 ? "checked":"")."/> </td>";
		echo "</tr>";			
		
		echo "<tr>";
		echo "<td class='tbl1' style='width: 50%; text-align: right; padding-right:5px;'> <strong>Dailymotion.com</strong> <br><span style='font-size: 9px;'>".$t[444]."</span> </td>";
		echo "<td class='tbl2' style='padding-left: 15px;'> <input type='checkbox' name='fdailymotion' ".($cinema['set_allow_dailymotion'] == 1 ? "checked":"")."/> </td>";
		echo "</tr>";	

		echo "<tr>";
		echo "<td class='tbl1' style='width: 50%; text-align: right; padding-right:5px;'> <strong>Metacafe.com</strong> <br><span style='font-size: 9px;'>".$t[445]."</span> </td>";
		echo "<td class='tbl2' style='padding-left: 15px;'> <input type='checkbox' name='fmetacafe' ".($cinema['set_allow_metacafe'] == 1 ? "checked":"")."/> </td>";
		echo "</tr>";	

		echo "<tr>";
		echo "<td class='tbl1' style='width: 50%; text-align: right; padding-right:5px;'> <strong>Break.com</strong> <br><span style='font-size: 9px;'>".$t[446]."</span> </td>";
		echo "<td class='tbl2' style='padding-left: 15px;'> <input type='checkbox' name='fbreak' ".($cinema['set_allow_break'] == 1 ? "checked":"")."/> </td>";
		echo "</tr>";	

		echo "<tr>";
		echo "<td class='tbl1' style='width: 50%; text-align: right; padding-right:5px;'> <strong>Redtube.com</strong> <br><span style='font-size: 9px;'>".$t[447]."</span> </td>";
		echo "<td class='tbl2' style='padding-left: 15px;'> <input type='checkbox' name='fredtube' ".($cinema['set_allow_redtube'] == 1 ? "checked":"")."/> <span style='position: absolute; margin-top: 4px; font-size: 9px; color: red; font-weight:bold;'>(18+)</span> </td>";
		echo "</tr>";	

		echo "<tr>";
		echo "<td class='tbl1' style='width: 50%; text-align: right; padding-right:5px;'> <strong>Pornhub.com</strong> <br><span style='font-size: 9px;'>".$t[448]."</span> </td>";
		echo "<td class='tbl2' style='padding-left: 15px;'> <input type='checkbox' name='fpornhub' ".($cinema['set_allow_pornhub'] == 1 ? "checked":"")."/> <span style='position: absolute; margin-top: 4px; font-size: 9px; color: red; font-weight:bold;'>(18+)</span> </td>";
		echo "</tr>";	

		echo "<tr>";
		echo "<td class='tbl2' colspan='2' style='font-size: 9px; color: red;' align='center'>".$t[449]."</td>";
		echo "</tr>";
		
		echo "<tr><td colspan='2' style='padding: 10px;'> <input type='submit' value='".$t[450]."' name='submitserver' class='button' /> </td></tr>";
	echo "</table>";
echo "</form>";
closetable();
	
opentable($cinema['set_name']." - ".$t[451]);
echo "<form action='' method='post'>";
	echo "<table border='0' align='center' width='100%' cellpadding='0' cellspacing='0'>";
		echo "<tr> <td colspan='2' class='forum-caption'> ".THEME_BULLET." ".$t[441].": </td></tr>";
		
		echo "<tr>";
		echo "<td class='tbl1' style='width: 50%; text-align: right; padding-right:5px;'> <strong>".$t[452].":</strong> <br><span style='font-size: 9px;'>".$t[453]."</span> </td>";
		echo "<td class='tbl2' style='padding-left: 15px;'> <input type='text' class='textbox' value='".$cinema['set_name']."' name='fname' style='width: 150px;' /> </td>";
		echo "</tr>";	
		
		echo "<tr>";
		echo "<td class='tbl1' style='width: 50%; text-align: right; padding-right:5px;'> <strong>".$t[454].":</strong> <br><span style='font-size: 9px;'>".$t[455]."</span> </td>";
		echo "<td class='tbl2' style='padding-left: 15px;'> <input type='text' class='textbox' value='".$cinema['set_index_panel_countgallery']."' name='fcountvideo' style='width: 50px;' /> </td>";
		echo "</tr>";		
		
		echo "<tr>";
		echo "<td class='tbl1' style='width: 50%; text-align: right; padding-right:5px;'> <strong>".$t[456].":</strong> <br><span style='font-size: 9px;'>".$t[457]."</span> </td>";
		echo "<td class='tbl2' style='padding-left: 15px;'> <input type='text' class='textbox' value='".$cinema['set_comments_limit']."' name='fcountcomments' style='width: 50px;' /> </td>";
		echo "</tr>";	

		echo "<tr>";
		echo "<td class='tbl1' style='width: 50%; text-align: right; padding-right:5px;'> <strong>".$t[458].":</strong> <br><span style='font-size: 9px;'>".$t[459]."</span> </td>";
		echo "<td class='tbl2' style='padding-left: 15px;'> <input type='text' class='textbox' value='".$cinema['set_comments_text_limit']."' name='fcountcommentstext' style='width: 50px;' /> </td>";
		echo "</tr>";				
		
		echo "<tr>";
		echo "<td class='tbl1' style='width: 50%; text-align: right; padding-right:5px;'> <strong>".$t[460].":</strong> <br><span style='font-size: 9px;'>".$t[461]."</span> </td>";
		echo "<td class='tbl2' style='padding-left: 15px;'> <input type='checkbox' name='fmaintenance' ".($cinema['set_maintenance'] == 1 ? "checked":"")."/> </td>";
		echo "</tr>";	
		
		echo "<tr>";
		echo "<td class='tbl1' style='width: 50%; text-align: right; padding-right:5px;'> <strong>".$t[462].":</strong> <br><span style='font-size: 9px;'>".$t[463]."</span> </td>";
		echo "<td class='tbl2' style='padding-left: 15px;'> <input type='checkbox' name='fwebalbum' ".($cinema['set_index_allow_web_album_panel'] == 1 ? "checked":"")."/> </td>";
		echo "</tr>";	
		
		echo "<tr>";
		echo "<td class='tbl1' style='width: 50%; text-align: right; padding-right:5px;'> <strong>".$t[464].":</strong> <br><span style='font-size: 9px;'>".$t[465]."</span> </td>";
		echo "<td class='tbl2' style='padding-left: 15px;'> <select name='fvideolist' class='textbox'> <option value='LIST' ".($cinema['set_video_list'] == 'LIST' ? "selected='selected'":"").">".$t[466]."</option> <option value='TABLE' ".($cinema['set_video_list'] == 'TABLE' ? "selected='selected'":"").">".$t[467]."</option> </select> </td>";
		echo "</tr>";	

		echo "<tr>";
		echo "<td class='tbl1' style='width: 50%; text-align: right; padding-right:5px;'> <strong>".$t[468].":</strong> <br><span style='font-size: 9px;'>".$t[469]."</span> </td>";
		echo "<td class='tbl2' style='padding-left: 15px;'> <select name='falbumlist' class='textbox'> <option value='LIST' ".($cinema['set_album_list'] == 'LIST' ? "selected='selected'":"").">".$t[466]."</option> <option value='TABLE' ".($cinema['set_album_list'] == 'TABLE' ? "selected='selected'":"").">".$t[467]."</option> </select> </td>";
		echo "</tr>";	
		
		echo "<tr>";
		echo "<td class='tbl1' style='width: 50%; text-align: right; padding-right:5px;'> <strong>".$t[470].":</strong> <br><span style='font-size: 9px;'>".$t[471]."</span> </td>";
		echo "<td class='tbl2' style='padding-left: 15px;'> <select name='flinkstyle' class='textbox'> <option value='SIMPLE' ".($cinema['set_cinema_link_style'] == 'SIMPLE' ? "selected='selected'":"").">".$t[472]." (djmCinema/?vid=1)</option> <option value='FULL' ".($cinema['set_cinema_link_style'] == 'FULL' ? "selected='selected'":"").">".$t[473]." (djmCinema/index.php?vid=1)</option> </select> </td>";
		echo "</tr>";	
		
		echo "<tr> <td colspan='2' class='forum-caption'> ".THEME_BULLET." ".$t[474].": </td></tr>";
		
		echo "<tr>";
		echo "<td class='tbl1' style='width: 50%; text-align: right; padding-right:5px;'> <strong>".$t[475].":</strong> <br><span style='font-size: 9px;'>".$t[476]."</span> </td>";
		echo "<td class='tbl2' style='padding-left: 15px;'> <input type='checkbox' name='faddvideo' ".($cinema['set_allow_add_video'] == 1 ? "checked":"")."/> </td>";
		echo "</tr>";		
		
		echo "<tr>";
		echo "<td class='tbl1' style='width: 50%; text-align: right; padding-right:5px;'> <strong>".$t[477].":</strong> <br><span style='font-size: 9px;'>".$t[478]."</span> </td>";
		echo "<td class='tbl2' style='padding-left: 15px;'> <input type='checkbox' name='facceptvideo' ".($cinema['set_allow_admin_accept'] == 1 ? "checked":"")."/> </td>";
		echo "</tr>";	
		
		echo "<tr>";
		echo "<td class='tbl1' style='width: 50%; text-align: right; padding-right:5px;'> <strong>".$t[479].":</strong> <br><span style='font-size: 9px;'>".$t[480]."</span> </td>";
		echo "<td class='tbl2' style='padding-left: 15px;'> <input type='checkbox' name='fcomments' ".($cinema['set_allow_comments'] == 1 ? "checked":"")."/> </td>";
		echo "</tr>";	
		
		echo "<tr>";
		echo "<td class='tbl1' style='width: 50%; text-align: right; padding-right:5px;'> <strong>".$t[481].":</strong> <br><span style='font-size: 9px;'>".$t[482]."</span> </td>";
		echo "<td class='tbl2' style='padding-left: 15px;'> <input type='checkbox' name='fratings' ".($cinema['set_allow_ratings'] == 1 ? "checked":"")."/> </td>";
		echo "</tr>";	
		
		echo "<tr>";
		echo "<td class='tbl1' style='width: 50%; text-align: right; padding-right:5px;'> <strong>".$t[483].":</strong> <br><span style='font-size: 9px;'>".$t[484]."</span> </td>";
		echo "<td class='tbl2' style='padding-left: 15px;'> <input type='text' class='textbox' name='fwidth' value='".$cinema['set_video_width']."' style='width: 50px;' /> px </td>";
		echo "</tr>";	
		
		echo "<tr>";
		echo "<td class='tbl1' style='width: 50%; text-align: right; padding-right:5px;'> <strong>".$t[485].":</strong> <br><span style='font-size: 9px;'>".$t[486]."</span> </td>";
		echo "<td class='tbl2' style='padding-left: 15px;'> <input type='text' class='textbox' name='fheight' value='".$cinema['set_video_height']."' style='width: 50px;' /> px </td>";
		echo "</tr>";	
		
		echo "<tr> <td colspan='2' class='forum-caption'> ".THEME_BULLET." ".$t[487].": </td></tr>";		
		
		echo "<tr>";
		echo "<td class='tbl1' style='width: 50%; text-align: right; padding-right:5px;'> <strong>".$t[488].":</strong> <br><span style='font-size: 9px;'>".$t[489]."</span> </td>";
		echo "<td class='tbl2' style='padding-left: 15px;'> <input type='checkbox' name='fcustomalbum' ".($cinema['set_allow_private_albums'] == 1 ? "checked":"")."/> </td>";
		echo "</tr>";			
		
		echo "<tr>";
		echo "<td class='tbl1' style='width: 50%; text-align: right; padding-right:5px;'> <strong>".$t[490].":</strong> <br><span style='font-size: 9px;'>".$t[491]."</span> </td>";
		echo "<td class='tbl2' style='padding-left: 15px;'> <input type='checkbox' name='ffavorites' ".($cinema['set_allow_favorites'] == 1 ? "checked":"")."/> </td>";
		echo "</tr>";	
		
		echo "<tr> <td colspan='2' class='forum-caption'> ".THEME_BULLET." ".$t[492].": </td></tr>";
		
		echo "<tr>"; 
		echo "<td class='tbl1' style='width: 50%; text-align: right; padding-right:5px;'> <strong>".$t[493].":</strong> <br><span style='font-size: 9px;'>".$t[494]."</span> </td>";
		echo "<td class='tbl2' style='padding-left: 15px;'> <select name='fcharset' class='textbox'> <option value='UTF8' ".($cinema['set_charset'] == 'UTF8' ? "selected='selected'":"").">UTF-8</option> <option value='WIN1250' ".($cinema['set_charset'] == 'WIN1250' ? "selected='selected'":"").">Windows-1250</option> </select> </td>";
		echo "</tr>";	

		echo "<tr>"; 
		echo "<td class='tbl1' style='width: 50%; text-align: right; padding-right:5px;'> <strong>".$t[495].":</strong> <br><span style='font-size: 9px;'>".$t[496]."</span> </td>";
		echo "<td class='tbl2' style='padding-left: 15px;'> <select name='ftranslatetype' class='textbox'> <option value='SYSTEM' ".($cinema['set_translate_type'] == 'SYSTEM' ? "selected='selected'":"").">".$t[497]."</option> <option value='CUSTOM' ".($cinema['set_translate_type'] == 'CUSTOM' ? "selected='selected'":"").">".$t[498]."</option> </select> </td>";
		echo "</tr>";		

		echo "<tr>"; 
		echo "<td class='tbl1' style='width: 50%; text-align: right; padding-right:5px;'> <strong>".$t[499].":</strong> <br><span style='font-size: 9px;'>".$t[500]."</span> </td>";
		echo "<td class='tbl2' style='padding-left: 15px;'> <select name='ftranslate' class='textbox'>";
		
				$temp = opendir(INFUSIONS."djmCinema/Locale/win1250/");
				$files = array();
				while ($folder = readdir($temp)) {
					if (!in_array($folder, array("..","."))) {
						echo "<option value='".str_replace(".php","",$folder)."' ".($cinema['set_translate'] == str_replace(".php","",$folder) ? "selected='selected'":"").">".str_replace(".php","",$folder)."</option>";
				 	}	
				}
 
		echo "</select> </td>"; 
		echo "</tr>";		
		
		echo "<tr> <td colspan='2' class='forum-caption'> ".THEME_BULLET." ".$t[501].": </td></tr>";		
		
		echo "<tr>";
		echo "<td class='tbl1' style='width: 50%; text-align: right; padding-right:5px;'> <strong>".$t[502].":</strong> <br><span style='font-size: 9px;'>".$t[503]."</span> </td>";
		echo "<td class='tbl2' style='padding-left: 15px;'>  #<input type='text' style='width: 55px;' value='".$cinema['set_border_color']."' name='fbordercolor' /> <span style='width: 5px; height: 5px; background-color: #".$cinema['set_border_color']."'>&nbsp;&nbsp;&nbsp;</span>  </td>";
		echo "</tr>";		

		echo "<tr>";
		echo "<td class='tbl2' colspan='2' style='padding: 10px;'><strong>".$t[504].":</strong> <br/> ".$t[505]."  <i>/infusions/djmCinema/Images/Icon/</i> ".$t[506]."  </td>";
		echo "</tr>";			
		
		echo "<tr>";
		echo "<td class='tbl1' style='width: 50%; text-align: right; padding-right:5px;'> <strong>".$t[507].":</strong> <br><span style='font-size: 9px;'>".$t[508]."</span> </td>";
		echo "<td class='tbl2' style='padding-left: 15px;'>  /Images/Icon/<input type='text' style='width: 105px;' value='".$cinema['set_album_img']."' name='falbumimg' /> <a href='".INFUSIONS."djmCinema/Images/Icon/".$cinema['set_album_img']."'><img src='".INFUSIONS."djmCinema/Images/Icon/".$cinema['set_album_img']."' alt='Album' style='width: 13px; height: 13px;' /></a>  </td>";
		echo "</tr>";	
		
		echo "<tr>";
		echo "<td class='tbl1' style='width: 50%; text-align: right; padding-right:5px;'> <strong>".$t[509].":</strong> <br><span style='font-size: 9px;'>".$t[510]."</span> </td>";
		echo "<td class='tbl2' style='padding-left: 15px;'>  /Images/Icon/<input type='text' style='width: 105px;' value='".$cinema['set_video_img']."' name='fvideoimg' /> <a href='".INFUSIONS."djmCinema/Images/Icon/".$cinema['set_video_img']."'><img src='".INFUSIONS."djmCinema/Images/Icon/".$cinema['set_video_img']."' alt='Album' style='width: 13px; height: 13px;' /></a>  </td>";
		echo "</tr>";
		
		echo "<tr>";
		echo "<td class='tbl1' style='width: 50%; text-align: right; padding-right:5px;'> <strong>".$t[511].":</strong> <br><span style='font-size: 9px;'>".$t[512]."</span> </td>";
		echo "<td class='tbl2' style='padding-left: 15px;'>  /Images/Icon/<input type='text' style='width: 105px;' value='".$cinema['set_lock_img']."' name='flockimg' /> <a href='".INFUSIONS."djmCinema/Images/Icon/".$cinema['set_lock_img']."'><img src='".INFUSIONS."djmCinema/Images/Icon/".$cinema['set_lock_img']."' alt='Album' style='width: 13px; height: 13px;' /></a>  </td>";
		echo "</tr>";
		
		echo "<tr> <td colspan='2' class='forum-caption'> ".THEME_BULLET." ".$t[513].": </td></tr>";
		
		echo "<tr>";
		echo "<td class='tbl1' style='width: 50%; text-align: right; padding-right:5px;'> <strong>Panel djmCinema:</strong> <br><span style='font-size: 9px;'> ".$t[514]." </span> </td>";
		echo "<td class='tbl2' style='padding-left: 15px;'> <a href='".ADMIN."panel_editor.php".$aidlink."&amp;panel_side=4'>".$t[515]."</a>  </td>";
		echo "</tr>";	

		echo "<tr>";
		echo "<td class='tbl1' style='width: 50%; text-align: right; padding-right:5px;'> <strong>".$t[516].":</strong> <br><span style='font-size: 9px;'> ".$t[517]." </span> </td>";
		echo "<td class='tbl2' style='padding-left: 15px;'> <select name='fpanelcontent' class='textbox'>
				<option value='1' ".($cinema['set_panel_content_type'] == 1 ? "selected='selected'":"").">".$t[518]."</option>
				<option value='2' ".($cinema['set_panel_content_type'] == 2 ? "selected='selected'":"").">".$t[519]."</option>
				<option value='3' ".($cinema['set_panel_content_type'] == 3 ? "selected='selected'":"").">".$t[520]."</option>
				<option value='4' ".($cinema['set_panel_content_type'] == 4 ? "selected='selected'":"").">".$t[521]."</option>
				<option value='5' ".($cinema['set_panel_content_type'] == 5 ? "selected='selected'":"").">".$t[522]."</option>
		</select> ".$t[523]." </td>";
		echo "</tr>";	

		echo "<tr>";
		echo "<td class='tbl1' style='width: 50%; text-align: right; padding-right:5px;'> <strong>".$t[524].":</strong> <br><span style='font-size: 9px;'>".$t[525]."</span> </td>";
		echo "<td class='tbl2' style='padding-left: 15px;'> <input type='text' class='textbox' name='fpanelcountvideo' value='".$cinema['set_panel_count_item']."' style='width: 30px;' /> </td>";
		echo "</tr>";			
		
		echo "<tr>";
		echo "<td class='tbl1' style='width: 50%; text-align: right; padding-right:5px;'> <strong>".$t[526].":</strong> <br><span style='font-size: 9px;'>".$t[527]."</span> </td>";
		echo "<td class='tbl2' style='padding-left: 15px;'> <input type='text' class='textbox' name='fpanelpadding' value='".$cinema['set_panel_padding']."' style='width: 30px;' />px </td>";
		echo "</tr>";				
		
		echo "<tr> <td colspan='2' class='forum-caption'> ".THEME_BULLET." ".$t[528].": </td></tr>";	
		
		echo "<tr>";
		echo "<td class='tbl1' valign='top' style='width: 50%; text-align: right; padding-right:5px;'> <strong>".$t[529].":</strong> <br><span style='font-size: 9px;'>".$t[530]."</span> </td>";
		echo "<td class='tbl2' style='padding-left: 15px;'> <textarea name='fbanreason' style=' width: 350px; height: 150px;' maxlength='100'>".$cinema['set_custom_bantext']."</textarea> </td>";
		echo "</tr>";	
		
		echo "<tr><td colspan='2' style='padding: 10px;'><input type='submit' value='".$t[531]."' name='submit' class='button'> </td></tr>";
	echo "</table>";
echo "</form>";
closetable();
?>