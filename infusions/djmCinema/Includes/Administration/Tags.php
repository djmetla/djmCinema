<?php
/*-------------------------------------------------------+
| PHP-Fusion Content Management System
| Copyright (C) 2002 - 2011 Nick Jones
| http://www.php-fusion.co.uk/
+--------------------------------------------------------+
| Filename: tags.php
| Author: Patrik Hoffmann (djmetla)
+--------------------------------------------------------+
| This program is released as free software under the
| Affero GPL license. You can redistribute it and/or
| modify it under the terms of this license which you
| can read by viewing the included agpl.txt or online
| at www.gnu.org/licenses/agpl.html. Removal of this
| copyright header is strictly prohibited without
| written permission from the original author(s).
+--------------------------------------------------------*/
if (!defined("IN_FUSION")) { die('Access Denied'); }
add_to_title("&nbsp;-&nbsp;".$cinema['set_name']." - ".$t[532]);

// New Tag
if (isset($_GET['new'])) {
opentable($t[533]);
echo "<form action='' method='post'>";
	echo "<table border='0' align='center' width='100%' cellpadding='0' cellspacing='0'>";
		echo "<tr>";
		echo "<td class='tbl1' style='width: 50%; text-align: right; padding-right:5px;'> <strong>* ".$t[534].": </strong> <br><span style='font-size: 9px;'>".$t[535]."</span> </td>";
		echo "<td class='tbl2' style='padding-left: 15px;'> <input type='text' class='textbox' name='tag_name' style='width: 150px;' /> </td>";
		echo "</tr>";
		echo "<tr>";
		echo "<td class='tbl1' style='width: 50%; text-align: right; padding-right:5px;'> <strong>".$t[536].":</strong> <br><span style='font-size: 9px;'>".$t[537]."</span> </td>";
		echo "<td class='tbl2' style='padding-left: 15px;'> <input type='checkbox' name='tag_restrict' /> </td>";
		echo "</tr>";			
		echo "<tr>";
		echo "<td class='tbl1' valign='top' style='width: 50%; text-align: right; padding-right:5px;'> <strong>".$t[538].":</strong> <br><span style='font-size: 9px;'>".$t[539]."</span> </td>";
		echo "<td class='tbl2' style='padding-left: 15px;'> <textarea name='tag_description' style=' width: 350px; height: 150px;'></textarea> </td>";
		echo "</tr>";
		echo "<tr><td colspan='2' style='padding: 10px;'><input type='submit' value='".$t[540]."' name='submit_new_tag' class='button'> ".$t[541]." <a href='".$redir['admin']."tags'>".$t[542]."</a> </td></tr>";	
	echo "</table>";
echo "</form>";

if (isset($_POST['submit_new_tag'])) {
  if (empty($_POST['tag_name'])) { redirect($redir['admin']."tags&amp;new&amp;error_blank"); }
  $form = array(
	'name' 			=> htmlspecialchars($_POST['tag_name']),
	'restrict'			=> ($_POST['tag_restrict'] ? 1:0),
	'description'		=> htmlspecialchars($_POST['tag_description'])
  );
  
  dbquery("INSERT INTO ".CIN_TAGS_VALUES." (tag_name, tag_description, tag_age_confirm) VALUES ('".$form['name']."','".$form['description']."','".$form['restrict']."')");
  redirect($redir['admin']."tags&amp;done_create");														
}
closetable();

// Edit Tag
}elseif (isset($_GET['edit']) && isnum($_GET['edit'])) {
 $sql = dbquery("SELECT * FROM ".CIN_TAGS_VALUES." WHERE tag_id='".$_GET['edit']."'");
 if (!dbrows($sql)) { redirect($redir['admin']."tags&amp;error_exist"); }
 $data = dbarray($sql);
opentable($t[543]);
echo "<form action='' method='post'>";
	echo "<input type='hidden' name='parent' value='".$data['tag_id']."' />";
	echo "<table border='0' align='center' width='100%' cellpadding='0' cellspacing='0'>";
		echo "<tr>";
		echo "<td class='tbl1' style='width: 50%; text-align: right; padding-right:5px;'> <strong>* ".$t[534].": </strong> <br><span style='font-size: 9px;'>".$t[535]."</span> </td>";
		echo "<td class='tbl2' style='padding-left: 15px;'> <input type='text' class='textbox' name='tag_name' value='".$data['tag_name']."' style='width: 150px;' /> </td>";
		echo "</tr>";
		echo "<tr>";
		echo "<td class='tbl1' style='width: 50%; text-align: right; padding-right:5px;'> <strong>".$t[536].":</strong> <br><span style='font-size: 9px;'>".$t[537]."</span> </td>";
		echo "<td class='tbl2' style='padding-left: 15px;'> <input type='checkbox' name='tag_restrict' ".($data['tag_age_confirm'] ? "checked":"")." /> </td>";
		echo "</tr>";			
		echo "<tr>";
		echo "<td class='tbl1' valign='top' style='width: 50%; text-align: right; padding-right:5px;'> <strong>".$t[538].":</strong> <br><span style='font-size: 9px;'>".$t[539]."</span> </td>";
		echo "<td class='tbl2' style='padding-left: 15px;'> <textarea name='tag_description' style=' width: 350px; height: 150px;'>".$data['tag_description']."</textarea> </td>";
		echo "</tr>";
		echo "<tr><td colspan='2' style='padding: 10px;'><input type='submit' value='".$t[543]."' name='submit_edit_tag' class='button'> ".$t[541]." <a href='".$redir['admin']."tags'>".$t[542]."</a> </td></tr>";	
	echo "</table>";
echo "</form>";
if (isset($_POST['submit_edit_tag'])) {
  if (empty($_POST['tag_name'])) { redirect($redir['admin']."tags&amp;edit=".$_POST['parent']."&amp;error_blank"); }
  $form = array(
	'name' 			=> htmlspecialchars($_POST['tag_name']),
	'restrict'		=> ($_POST['tag_restrict'] ? 1:0),
	'description'	=> htmlspecialchars($_POST['tag_description'])
  );
  dbquery("UPDATE ".CIN_TAGS_VALUES." SET tag_name='".$form['name']."', tag_description='".$form['description']."', tag_age_confirm='".$form['restrict']."' WHERE tag_id='".$_POST['parent']."'");
  redirect($redir['admin']."tags&amp;done_update");														
}
unset($sql, $data);

// Delete Tags
}elseif (isset($_GET['delete']) && isnum($_GET['delete'])) {	
  $video_exist = dbcount("(*)", CIN_TAGS, "tag_tag='".$_GET['delete']."'");
  if ($video_exist != 0) { dbquery("DELETE FROM ".CIN_TAGS." WHERE tag_tag='".$_GET['delete']."'");	}	
  dbquery("DELETE FROM ".CIN_TAGS_VALUES." WHERE tag_id='".$_GET['delete']."'");
  redirect($redir['admin']."tags&amp;done_delete");  	
}

opentable($cinema['set_name']." - ".$t[532]);
	echo "<table border='0' align='center' width='100%' cellpadding='0' cellspacing='1'>";
		echo "<tr> <td colspan='5' class='forum-caption'> ".THEME_BULLET." ".$t[544].": </td></tr>";
		echo "<tr>";
			echo "<td class='tbl2' colspan='5'> <a href='".$redir['admin']."tags&amp;new'>".$t[545]."</a> </td>";
		echo "</tr>";	
		echo "<tr> <td colspan='5' class='forum-caption'> ".THEME_BULLET." ".$t[532].": </td></tr>";
		$sql = dbquery("SELECT * FROM ".CIN_TAGS_VALUES);
		if (!dbrows($sql)) { echo "<tr><td class='tbl1' align='center' colspan='2'>".$t[546]."</td></tr>"; }
		while ($tags = dbarray($sql)) {
			$vcount = dbcount("(*)", CIN_TAGS, "tag_tag='".$tags['tag_id']."'");
			echo "<tr>";
				echo "<td class='tbl1'> ".$tags['tag_name']." </td>";
				echo "<td class='tbl2' align='center'> ".($tags['tag_age_confirm'] ? $t[548]:"--")."  </td>";
				echo "<td class='tbl1' align='center'> ".$t[547].": ".$vcount." </td>";
				echo "<td class='tbl2' align='center'> <a href='".$redir['admin']."tags&amp;edit=".$tags['tag_id']."'>".$t[549]."</a> &middot; <a href='#' onclick=\"if(confirm(' ".($vcount == 0 ? " ".$t[550]." ".$tags['tag_name']." ?":$t[551]." (".$vcount.") ".$t[552])."  ')) document.location='".$redir['admin']."tags&amp;delete=".$tags['tag_id']."'\">".$t[553]."</a>  </td>";
			echo "</tr>";		
		}	
	echo "</table>";	
closetable();
?>