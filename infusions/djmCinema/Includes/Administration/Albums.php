<?php
/*-------------------------------------------------------+
| PHP-Fusion Content Management System
| Copyright (C) 2002 - 2011 Nick Jones
| http://www.php-fusion.co.uk/
+--------------------------------------------------------+
| Filename: albums.php
| Author: Patrik Hoffmann (djmetla)
+--------------------------------------------------------+
| This program is released as free software under the
| Affero GPL license. You can redistribute it and/or
| modify it under the terms of this license which you
| can read by viewing the included agpl.txt or online
| at www.gnu.org/licenses/agpl.html. Removal of this
| copyright header is strictly prohibited without
| written permission from the original author(s).
+--------------------------------------------------------*/
if (!defined("IN_FUSION")) { die('Access Denied'); }
add_to_title("&nbsp;-&nbsp;".$cinema['set_name']." - ".$t[344]);

// New Album
if (isset($_GET['new'])) {
opentable($t[345]);
echo "<form action='' method='post'>";
	echo "<table border='0' align='center' width='100%' cellpadding='0' cellspacing='0'>";
		echo "<tr>";
		echo "<td class='tbl1' style='width: 50%; text-align: right; padding-right:5px;'> <strong>* ".$t[346].": </strong> <br><span style='font-size: 9px;'>".$t[347]."</span> </td>";
		echo "<td class='tbl2' style='padding-left: 15px;'> <input type='text' class='textbox' name='album_name' style='width: 150px;' /> </td>";
		echo "</tr>";
		echo "<tr>";
		echo "<td class='tbl1' style='width: 50%; text-align: right; padding-right:5px;'> <strong>".$t[348].":</strong> <br><span style='font-size: 9px;'>".$t[349]."</span> </td>";
		echo "<td class='tbl2' style='padding-left: 15px;'> <input type='text' class='textbox' name='album_password' style='width: 150px;' /> </td>";
		echo "</tr>";
		echo "<tr>";
		echo "<td class='tbl1' style='width: 50%; text-align: right; padding-right:5px;'> <strong>".$t[350].":</strong> <br><span style='font-size: 9px;'>".$t[351]."</span> </td>";
		echo "<td class='tbl2' style='padding-left: 15px;'> <input type='checkbox' name='album_insert'/> </td>";
		echo "</tr>";	
		echo "<tr>";
		echo "<td class='tbl1' valign='top' style='width: 50%; text-align: right; padding-right:5px;'> <strong>".$t[353].":</strong> <br><span style='font-size: 9px;'>".$t[352]."</span> </td>";
		echo "<td class='tbl2' style='padding-left: 15px;'> <textarea name='album_description' style=' width: 350px; height: 150px;'></textarea> </td>";
		echo "</tr>";
		echo "<tr><td colspan='2' style='padding: 10px;'><input type='submit' value='".$t[354]."' name='submit_new_album' class='button'> ".$t[355]." <a href='".$redir['admin']."albums'>".$t[356]."</a> </td></tr>";	
	echo "</table>";
echo "</form>";

if (isset($_POST['submit_new_album'])) {
  if (empty($_POST['album_name'])) { redirect($redir['admin']."albums&amp;new&amp;error_blank"); }
  $form = array(
	'name' 				=> htmlspecialchars($_POST['album_name']),
	'password'		=> (!empty($_POST['album_password']) ?  $_POST['album_password']:''),
	'description'		=> htmlspecialchars($_POST['album_description']),
	'insert'				=> (isset($_POST['album_insert']) ? 1 : 0),
	'time'				=> time(),
	'user'				=> $userdata['user_id'],
	'type'				=> 'PUBLIC'
  );
  
  dbquery("INSERT INTO ".CIN_GALLERY." (gallery_name, gallery_user, gallery_time, gallery_type, gallery_password, gallery_allow_insert, gallery_description) VALUES ('".$form['name']."','".$form['user']."','".$form['time']."','".$form['type']."','".$form['password']."','".$form['insert']."','".$form['description']."')");
  redirect($redir['admin']."albums&amp;done_create");														
}
closetable();

// Edit Album
}elseif (isset($_GET['edit']) && isnum($_GET['edit'])) {
 $sql = dbquery("SELECT * FROM ".CIN_GALLERY." WHERE gallery_id='".$_GET['edit']."'");
 if (!dbrows($sql)) { redirect($redir['admin']."albums&amp;error_exist"); }
 $data = dbarray($sql);
opentable($t[357]);
echo "<form action='' method='post'>";
	echo "<input type='hidden' name='parent' value='".$data['gallery_id']."' />";
	echo "<table border='0' align='center' width='100%' cellpadding='0' cellspacing='0'>";
		echo "<tr>";
		echo "<td class='tbl1' style='width: 50%; text-align: right; padding-right:5px;'> <strong>* ".$t[358].": </strong> <br><span style='font-size: 9px;'>".$t[359]."</span> </td>";
		echo "<td class='tbl2' style='padding-left: 15px;'> <input type='text' class='textbox' value='".$data['gallery_name']."' name='album_name' style='width: 150px;' /> </td>";
		echo "</tr>";
		echo "<tr>";
		echo "<td class='tbl1' style='width: 50%; text-align: right; padding-right:5px;'> <strong>".$t[360].":</strong> <br><span style='font-size: 9px;'>".$t[349]."</span> </td>";
		echo "<td class='tbl2' style='padding-left: 15px;'> <input type='text' class='textbox'  value='".$data['gallery_password']."' name='album_password' style='width: 150px;' /> </td>";
		echo "</tr>";
		echo "<tr>";
		echo "<td class='tbl1' style='width: 50%; text-align: right; padding-right:5px;'> <strong>".$t[350].":</strong> <br><span style='font-size: 9px;'>".$t[351]."</span> </td>";
		echo "<td class='tbl2' style='padding-left: 15px;'> <input type='checkbox' name='album_insert'   ".($data['gallery_allow_insert'] == 1 ? "checked":"")."/> </td>";
		echo "</tr>";			
		echo "<tr>";
		echo "<td class='tbl1' valign='top' style='width: 50%; text-align: right; padding-right:5px;'> <strong>".$t[353].":</strong> <br><span style='font-size: 9px;'>".$t[352]."</span> </td>";
		echo "<td class='tbl2' style='padding-left: 15px;'> <textarea name='album_description' style=' width: 350px; height: 150px;'>".$data['gallery_description']."</textarea> </td>";
		echo "</tr>";
		echo "<tr><td colspan='2' style='padding: 10px;'><input type='submit' value='".$t[357]."' name='submit_edit_album' class='button'> ".$t[355]." <a href='".$redir['admin']."albums'>".$t[356]."</a> </td></tr>";	
	echo "</table>";
echo "</form>";
if (isset($_POST['submit_edit_album'])) {
  if (empty($_POST['album_name'])) { redirect($redir['admin']."albums&amp;edit=".$_POST['parent']."&amp;error_blank"); }
  $form = array(
	'name' 			=> htmlspecialchars($_POST['album_name']),
	'password'		=> (!empty($_POST['album_password']) ?  $_POST['album_password']:''),
	'insert'				=> (isset($_POST['album_insert']) ?  1 : 0),
	'description'	=> htmlspecialchars($_POST['album_description'])
  );
  dbquery("UPDATE ".CIN_GALLERY." SET gallery_name='".$form['name']."', gallery_password='".$form['password']."', gallery_allow_insert='".$form['insert']."', gallery_description='".$form['description']."' WHERE gallery_id='".$_POST['parent']."'");
  redirect($redir['admin']."albums&amp;done_update");														
}
unset($sql, $data);

// Delete Album
}elseif (isset($_GET['delete']) && isnum($_GET['delete'])) {	
  $video_exist = dbcount("(*)", CIN_VIDEO, "video_gallery='".$_GET['delete']."'");
  if ($video_exist != 0) { dbquery("DELETE FROM ".CIN_VIDEO." WHERE video_gallery='".$_GET['delete']."'");	}	
  dbquery("DELETE FROM ".CIN_GALLERY." WHERE gallery_id='".$_GET['delete']."'");
  redirect($redir['admin']."albums&amp;done_delete");  	
}

opentable($cinema['set_name']." - ".$t[344]); 
	echo "<table border='0' align='center' width='100%' cellpadding='0' cellspacing='1'>";
		echo "<tr> <td colspan='5' class='forum-caption'> ".THEME_BULLET." ".$t[361].": </td></tr>";
		echo "<tr>";
			echo "<td class='tbl2' colspan='5'> <a href='".$redir['admin']."albums&amp;new'>".$t[362]."</a> &middot; <a href='".$redir['admin']."settings'>".$t[363]."</a> </td>";
		echo "</tr>";	
		echo "<tr> <td colspan='5' class='forum-caption'> ".THEME_BULLET." ".$t[364].": </td></tr>";
		$sql = dbquery("SELECT t1.*, t2.* FROM ".CIN_GALLERY." as t1 LEFT JOIN ".DB_PREFIX."users as t2 ON t2.user_id=t1.gallery_user WHERE gallery_type='PUBLIC'");
		if (!dbrows($sql)) { echo "<tr><td class='tbl1' align='center' colspan='2'>".$t[365]."</td></tr>"; }
		while ($gallery = dbarray($sql)) {
			$vcount = dbcount("(*)", CIN_VIDEO, "video_gallery='".$gallery['gallery_id']."'");
			echo "<tr>";
				echo "<td class='tbl1'> <a href='".$redir['album'].$gallery['gallery_id']."'>".$gallery['gallery_name']."</a> ".($gallery['gallery_password'] != '' ? "(".$t[366].": ".$gallery['gallery_password'].")":"")." </td>";
				echo "<td class='tbl2'> ".$t[367].": <a href='".BASEDIR."profile.php?lookup=".$gallery['user_id']."'>".$gallery['user_name']."</a> </td>";
				echo "<td class='tbl1' align='center'> ".$t[368].": ".strftime("%d.%m.%Y o %H:%M", $gallery['gallery_time'])." </td>";
				echo "<td class='tbl2' align='center'> ".$t[369].": ".$vcount." </td>";
				echo "<td class='tbl1' align='center'> <a href='".$redir['admin']."albums&amp;edit=".$gallery['gallery_id']."'>".$t[370]."</a> &middot; <a href='#' onclick=\"if(confirm(' ".($vcount == 0 ? " ".$t[371]." ".$gallery['gallery_name']." ?":$t[372]." (".$vcount.") ".$t[373])."  ')) document.location='".$redir['admin']."albums&amp;delete=".$gallery['gallery_id']."'\">".$t[374]."</a>  </td>";
			echo "</tr>";		
		}	
	echo "</table>";	
closetable();
?>