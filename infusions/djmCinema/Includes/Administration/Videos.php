<?php
/*-------------------------------------------------------+
| PHP-Fusion Content Management System
| Copyright (C) 2002 - 2011 Nick Jones
| http://www.php-fusion.co.uk/
+--------------------------------------------------------+
| Filename: reports.php
| Author: Patrik Hoffmann (djmetla)
+--------------------------------------------------------+
| This program is released as free software under the
| Affero GPL license. You can redistribute it and/or
| modify it under the terms of this license which you
| can read by viewing the included agpl.txt or online
| at www.gnu.org/licenses/agpl.html. Removal of this
| copyright header is strictly prohibited without
| written permission from the original author(s).
+--------------------------------------------------------*/
if (!defined("IN_FUSION")) { die('Access Denied'); }

add_to_title("&nbsp;-&nbsp;".$cinema['set_name']." - ".$t[575]);

	// Search
	if (isset($_GET['q']) && !empty($_GET['q'])) {
	
		opentable($cinema['set_name']." - ".$t[576]);
		echo "<table border='0' align='center' width='100%' cellpadding='0' cellspacing='1'>";
			$sql = dbquery("SELECT t1.*, t2.user_name FROM ".CIN_VIDEO." as t1 LEFT JOIN ".DB_PREFIX."users as t2 ON t2.user_id=t1.video_user WHERE video_name LIKE '%".$_GET['q']."%' OR video_id LIKE '%".$_GET['q']."%' OR video_source LIKE '%".$_GET['q']."%' OR user_name LIKE '%".$_GET['q']."%'");
			if (!dbrows($sql)) { echo "<tr><td class='tbl1' align='center' colspan='2'> ".$t[577]." </td></tr>"; }
			while ($data = dbarray($sql)) {
				echo "<tr>";
					echo "<td class='tbl1'> ".$data['video_name']." </td>";
					echo "<td class='tbl2'> ".$t[578].": <a href='".BASEDIR."profile.php?lookup=".$data['video_user']."'>".$data['user_name']."</a>  </td>";
					echo "<td class='tbl1' align='center'> ".$t[579].": ".$data['video_viewed'].", ".$t[580].":  ".($data['video_status'] == 1 ? "<span style='color: green;'>".$t[581]."</span>":"")." </td>";
					echo "<td class='tbl2' align='center'> <a href='".$redir['admin']."videos&amp;edit=".$data['video_id']."'>".$t[582]."</a> &nbsp; <a href='#' onclick=\"if(confirm(' ".$t[583]." ".$data['video_name']." ".$t[584]."  ')) document.location='".$redir['admin']."videos&amp;block=".$data['video_id']."'\">".$t[585]."</a> &nbsp; <a href='#' onclick=\"if(confirm(' ".$t[586]." ".$data['video_name']." ?  ')) document.location='".$redir['admin']."videos&amp;delete=".$data['video_id']."'\">".$t[587]."</a>  </td>";
				echo "</tr>";		
			}	
		echo "</table>";	
		unset($sql);
		closetable();
		
		
	// Accept video
    }elseif (isset($_GET['accept']) && isnum($_GET['accept'])) {	
	
		$control = dbquery("SELECT * FROM ".CIN_VIDEO." WHERE video_id='".$_GET['accept']."'");
		if (dbrows($control)) {
			$data = dbarray($control);
			$message = $t[588]." ".$data['video_name']." ".$t[589]." ".$userdata['user_name'];
			$subject = $t[590];
			dbquery("INSERT INTO ".DB_PREFIX."messages (message_to, message_from, message_subject, message_message, message_datestamp) VALUES ('".$data['video_user']."','".$userdata['user_id']."','".$subject."','".$message."','".time()."')");
			dbquery("UPDATE ".CIN_VIDEO." SET video_status='1' WHERE video_id='".$_GET['accept']."'");
			redirect($redir['admin']."videos&amp;done_accept");
		} else { redirect($redir['admin']."videos"); }
	
	// Edit video
    }elseif (isset($_GET['edit']) && isnum($_GET['edit'])) {

		$control = dbquery("SELECT * FROM ".CIN_VIDEO." WHERE video_id='".$_GET['edit']."' ");
		if (!dbrows($control)) { redirect($redir['admin']."videos"); }
		$data = dbarray($control);
		
		opentable($t[591]." <b>".$data['video_name']."</b>");
		echo "<form action='' method='post'>";
			echo "<table border='0' align='center' width='100%' cellpadding='0' cellspacing='1'>";
				echo "<tr>";
				echo "<td class='tbl1' style='width: 40%; text-align: right; padding-right:5px;'> <strong>* ".$t[592].": </strong> <br><span style='font-size: 9px;'>".$t[593]."</span> </td>";
				echo "<td class='tbl2' style='padding-left: 15px;'> <input type='text' class='textbox' name='video_name' value='".$data['video_name']."' style='width: 200px;' /> </td>";
				echo "</tr>";		
				
				if ($data['video_comment'] == 1) { $comment_checked = "checked"; }else{ $comment_checked = ""; }
				echo "<tr>";
				echo "<td class='tbl1' style='width: 40%; text-align: right; padding-right:5px;'> <strong> ".$t[594].": </strong> <br><span style='font-size: 9px;'>".$t[595]."</span> </td>";
				echo "<td class='tbl2' style='padding-left: 15px;'> <input type='checkbox' name='video_comments' ".($cinema['set_allow_comments'] == 1 ? $comment_checked:"disabled")."/> <i> ".($cinema['set_allow_comments'] == 0 ? "(<i>".$t[596]."</i>)":"")."</i> </td>";
				echo "</tr>";

				if ($data['video_rating'] == 1) { $rating_checked = "checked"; }else{ $rating_checked = ""; }
				echo "<tr>";
				echo "<td class='tbl1' style='width: 40%; text-align: right; padding-right:5px;'> <strong> ".$t[597].": </strong> <br><span style='font-size: 9px;'>".$t[598] ."</span> </td>";
				echo "<td class='tbl2' style='padding-left: 15px;'> <input type='checkbox' name='video_ratings' ".($cinema['set_allow_ratings'] == 1 ? $rating_checked:"disabled")."/> ".($cinema['set_allow_ratings'] == 0 ? "(<i>".$t[599]."</i>)":"")." </td>";
				echo "</tr>";	

				echo "<tr>";
				echo "<td class='tbl1' valign='top' style='width: 40%; text-align: right; padding-right:5px;'> <strong> ".$t[600].": </strong> <br><span style='font-size: 9px;'>".$t[601]."</span> </td>";
				echo "<td class='tbl2' style='padding-left: 15px;'>";
						
						$sql_tags = dbquery("SELECT * FROM ".CIN_TAGS_VALUES);
						if (dbrows($sql_tags)) {
							while ($tags = dbarray($sql_tags)) {
								$tag_exist = dbcount("(*)", CIN_TAGS, "tag_video='".$data['video_id']."' AND tag_tag='".$tags['tag_id']."' ");
								echo "<input type='checkbox' name='video_tag[]' value='".$tags['tag_id']."' ".($tag_exist != 0 ? "checked":"")."  /> <span style='position:absolute; margin-top: 3px;'>".$tags['tag_name']."</span> <br/>";
							}
						}else{
							echo "<i>".$t[602]."</i>";
						}
				echo "</td>";
				echo "</tr>";			

				echo "<tr>";
				echo "<td class='tbl1' valign='top' style='width: 50%; text-align: right; padding-right:5px;'> <strong>".$t[603].":</strong> <br><span style='font-size: 9px;'>".$t[604]."</span> </td>";
				echo "<td class='tbl2' style='padding-left: 15px;'> <textarea name='video_description' style=' width: 350px; height: 150px;'>".$data['video_description']."</textarea> </td>";
				echo "</tr>";	

				echo "<tr><td colspan='2' style='padding: 10px;'><input type='submit' value='".$t[605]."' name='submit_edit' class='button'> ".$t[606]." <a href='".$redir['admin']."videos'>".$t[607]."</a> </td></tr>";
			echo "</table>";
		echo "</form>";	
		closetable();
		
		if (isset($_POST['submit_edit'])) {
			
			$form = array(
				'name' => (empty($_POST['video_name']) ? $data['video_name']:$_POST['video_name']),
				'comment' => (isset($_POST['video_comments']) ? 1:0),
				'rating' => (isset($_POST['video_ratings']) ? 1:0),
				'description' => htmlspecialchars($_POST['video_description'])
			);
			
			
			// Tags core 
			dbquery("DELETE FROM ".CIN_TAGS." WHERE tag_video='".$data['video_id']."'");
			for ($i = 0;$i < count($_POST['video_tag']);$i++) {
				$tag = $_POST['video_tag'][$i];
				$sql = dbquery("SELECT * FROM ".CIN_TAGS_VALUES." WHERE tag_id='".$tag."' ");
				$security = dbcount("(*)", CIN_TAGS, "tag_video='".$data['video_id']."' AND tag_tag='".$tag."'");
				if (dbrows($sql) && !$security) {
					dbquery("INSERT INTO ".CIN_TAGS." (tag_video, tag_tag) VALUES ('".$data['video_id']."','".$tag."')");
				}
			}

			dbquery("UPDATE ".CIN_VIDEO." SET video_name='".$form['name']."', video_comment='".$form['comment']."', video_rating='".$form['rating']."', video_description='".$form['description']."' WHERE video_id='".$data['video_id']."' ");
			redirect($redir['admin']."videos&amp;edit=".$data['video_id']."&amp;done_update");
		}

	
	// Block video
    }elseif (isset($_GET['block']) && isnum($_GET['block'])) {
	
		$control = dbquery("SELECT * FROM ".CIN_VIDEO." WHERE video_id='".$_GET['block']."' ");
		if (dbrows($control)) {
			$data = dbarray($control);
			$message = $t[608]." ".$data['video_name']." ".$t[609]." ".$userdata['user_name'];
			$subject = $t[610];
			dbquery("INSERT INTO ".DB_PREFIX."messages (message_to, message_from, message_subject, message_message, message_datestamp) VALUES ('".$data['video_user']."','".$userdata['user_id']."','".$subject."','".$message."','".time()."')");
			dbquery("UPDATE ".CIN_VIDEO." SET video_status='2' WHERE video_id='".$_GET['block']."'");
			redirect($redir['admin']."videos&amp;done_block");
		} else { redirect($redir['admin']."videos"); }	
		
	// Unblock video
    }elseif (isset($_GET['unblock']) && isnum($_GET['unblock'])) {
	
		$control = dbquery("SELECT * FROM ".CIN_VIDEO." WHERE video_id='".$_GET['unblock']."'");
		if (dbrows($control)) {
			$data = dbarray($control);
			$message = $t[611]." ".$data['video_name']." ".$t[612]." ".$userdata['user_name'];
			$subject = $t[613];
			dbquery("INSERT INTO ".DB_PREFIX."messages (message_to, message_from, message_subject, message_message, message_datestamp) VALUES ('".$data['video_user']."','".$userdata['user_id']."','".$subject."','".$message."','".time()."')");
			dbquery("UPDATE ".CIN_VIDEO." SET video_status='1' WHERE video_id='".$_GET['unblock']."'");
			redirect($redir['admin']."videos&amp;done_unblock");
		} else { redirect($redir['admin']."videos"); }		
		
	// Delete video
    }elseif (isset($_GET['delete']) && isnum($_GET['delete'])) {	
	
		$control = dbquery("SELECT * FROM ".CIN_VIDEO." WHERE video_id='".$_GET['delete']."'");
		if (dbrows($control)) {	
			$data = dbarray($control);
			$message = $t[614]." ".$data['video_name']." ".$t[615]." ".$userdata['user_name'];
			$subject = $t[616];
			dbquery("INSERT INTO ".DB_PREFIX."messages (message_to, message_from, message_subject, message_message, message_datestamp) VALUES ('".$data['video_user']."','".$userdata['user_id']."','".$subject."','".$message."','".time()."')");
			dbquery("DELETE FROM ".CIN_TAG." WHERE tag_video='".$_GET['delete']."'");
			dbquery("DELETE FROM ".CIN_COMMENT." WHERE comment_item='".$_GET['delete']."'");
			dbquery("DELETE FROM ".CIN_RATING." WHERE rating_video='".$_GET['delete']."'");
			dbquery("DELETE FROM ".CIN_REPORT." WHERE report_video='".$_GET['delete']."'");
			dbquery("DELETE FROM ".CIN_VIDEO." WHERE video_id='".$_GET['delete']."'");
			redirect($redir['admin']."videos&amp;done_delete");
		} else { redirect($redir['admin']."videos"); }	
		
		
	} else {

		opentable($cinema['set_name']." - ".$t[617]);
		echo "<div style='text-align:center; padding: 10px;'>";
			echo "<form name='search' action='admin.php' method='get'> <input type='hidden' name='aid' value='".iAUTH."' /> <input type='hidden' name='p' value='videos'>";
			 echo "<input id='cinemaSearch' type='text' name='q' data-defined='".$t[618]."' class='textbox' style=' padding: 7px; width: 325px;' /> &nbsp;&nbsp;&nbsp; <input id='cinemaSearchSubmit' style='padding: 7px; display: none;' type='submit' class='button' value='".$t[619]."'/>";
			echo "</form>";
		echo "</div>";
		closetable();
		
		opentable($cinema['set_name']." - ".$t[620]);
		echo "<table border='0' align='center' width='100%' cellpadding='0' cellspacing='1'>";
			$sql = dbquery("SELECT t1.*, t2.user_name FROM ".CIN_VIDEO." as t1 LEFT JOIN ".DB_PREFIX."users as t2 ON t2.user_id=t1.video_user WHERE video_status='0'");
			if (!dbrows($sql)) { echo "<tr><td class='tbl1' align='center' colspan='2'> ".$t[621]." </td></tr>"; }
			while ($data = dbarray($sql)) {
				echo "<tr>";
					echo "<td class='tbl1'> ".$data['video_name']." </td>";
					echo "<td class='tbl2'> ".$t[622].": <a href='".BASEDIR."profile.php?lookup=".$data['video_user']."'>".$data['user_name']."</a>  </td>";
					echo "<td class='tbl1' align='center'> ".$t[623].": ".$data['video_viewed'].", ".$t[624].":  ".($data['video_status'] == 0 ? "<span style='color: orange;'>".$t[625]."</span>":"")." </td>";
					echo "<td class='tbl2' align='center'> <a href='".$redir['video'].$data['video_id']."'>".$t[626]."</a> </td>";
				echo "</tr>";		
			}	
		echo "</table>";	
		unset($sql);
		closetable();
		
		opentable($cinema['set_name']." - ".$t[627]);
		echo "<table border='0' align='center' width='100%' cellpadding='0' cellspacing='1'>";
			$sql = dbquery("SELECT t1.*, t2.user_name FROM ".CIN_VIDEO." as t1 LEFT JOIN ".DB_PREFIX."users as t2 ON t2.user_id=t1.video_user WHERE video_status='2'");
			if (!dbrows($sql)) { echo "<tr><td class='tbl1' align='center' colspan='2'> ".$t[621]." </td></tr>"; }
			while ($data = dbarray($sql)) {
				echo "<tr>";
					echo "<td class='tbl1'> ".$data['video_name']." </td>";
					echo "<td class='tbl2'> ".$t[622].": <a href='".BASEDIR."profile.php?lookup=".$data['video_user']."'>".$data['user_name']."</a>  </td>";
					echo "<td class='tbl1' align='center'> ".$t[623].": ".$data['video_viewed'].", ".$t[624].":  ".($data['video_status'] == 2 ? "<span style='color: red;'>".$t[628]."</span>":"")." </td>";
					echo "<td class='tbl2' align='center'> <a href='".$redir['admin']."videos&amp;edit=".$data['video_id']."'>".$t[629]."</a> &middot; <a href='#' onclick=\"if(confirm(' ".$t[630]." ".$data['video_name']." ".$t[631]."  ')) document.location='".$redir['admin']."videos&amp;unblock=".$data['video_id']."'\">".$t[632]."</a> &middot; <a href='#' onclick=\"if(confirm(' ".$t[633]." ".$data['video_name']." ?  ')) document.location='".$redir['admin']."videos&amp;delete=".$data['video_id']."'\">".$t[634]."</a>  </td>";
				echo "</tr>";		
			}	
		echo "</table>";	
		unset($sql);
		closetable();		
		
		opentable($cinema['set_name']." - ".$t[635]);
		echo "<table border='0' align='center' width='100%' cellpadding='0' cellspacing='1'>";
			$sql = dbquery("SELECT t1.*, t2.user_name FROM ".CIN_VIDEO." as t1 LEFT JOIN ".DB_PREFIX."users as t2 ON t2.user_id=t1.video_user WHERE video_status='1' LIMIT 20");
			if (!dbrows($sql)) { echo "<tr><td class='tbl1' align='center' colspan='2'> ".$t[621]." </td></tr>"; }
			while ($data = dbarray($sql)) {
				echo "<tr>";
					echo "<td class='tbl1'> ".$data['video_name']." </td>";
					echo "<td class='tbl2'> ".$t[622].": <a href='".BASEDIR."profile.php?lookup=".$data['video_user']."'>".$data['user_name']."</a>  </td>";
					echo "<td class='tbl1' align='center'> ".$t[623].": ".$data['video_viewed'].", ".$t[624].":  ".($data['video_status'] == 1 ? "<span style='color: green;'>".$t[636]."</span>":"")." </td>";
					echo "<td class='tbl2' align='center'> <a href='".$redir['admin']."videos&amp;edit=".$data['video_id']."'>".$t[629]."</a> &middot; <a href='#' onclick=\"if(confirm(' ".$t[637]." ".$data['video_name']." ".$t[638]."  ')) document.location='".$redir['admin']."videos&amp;block=".$data['video_id']."'\">".$t[639]."</a> &middot; <a href='#' onclick=\"if(confirm(' ".$t[640]." ".$data['video_name']." ?  ')) document.location='".$redir['admin']."videos&amp;delete=".$data['video_id']."'\">".$t[641]."</a>  </td>";
				echo "</tr>";		
			}	
		echo "</table>";	
		unset($sql);
		closetable();
	}

?>