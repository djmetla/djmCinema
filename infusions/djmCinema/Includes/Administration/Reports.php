<?php
/*-------------------------------------------------------+
| PHP-Fusion Content Management System
| Copyright (C) 2002 - 2011 Nick Jones
| http://www.php-fusion.co.uk/
+--------------------------------------------------------+
| Filename: reports.php
| Author: Patrik Hoffmann (djmetla)
+--------------------------------------------------------+
| This program is released as free software under the
| Affero GPL license. You can redistribute it and/or
| modify it under the terms of this license which you
| can read by viewing the included agpl.txt or online
| at www.gnu.org/licenses/agpl.html. Removal of this
| copyright header is strictly prohibited without
| written permission from the original author(s).
+--------------------------------------------------------*/
if (!defined("IN_FUSION")) { die('Access Denied'); }
add_to_title("&nbsp;-&nbsp;".$cinema['set_name']." - ".$t[422]);

	// View
	if (isset($_GET['work']) && isnum($_GET['work'])) {
	
		$sql = dbquery("SELECT t1.*,t2.user_name as reporter, t3.*, t4.user_name as creator FROM ".CIN_REPORT." as t1 LEFT JOIN ".DB_PREFIX."users as t2 ON t2.user_id=t1.report_user LEFT JOIN ".CIN_VIDEO." as t3 ON t3.video_id=t1.report_video LEFT JOIN ".DB_PREFIX."users as t4 ON t4.user_id=t3.video_user WHERE report_id='".$_GET['work']."'");
		if (!dbrows($sql))  redirect($redir['admin']."reports"); 
		$data = dbarray($sql);
		
		opentable($t[423]." <b>".$data['video_name']."</b>");
		echo "<table border='0' align='center' width='100%' cellpadding='0' cellspacing='1'>";
			echo "<tr>";
			echo "<td class='tbl1' style='width: 50%; text-align: right; padding-right:5px;'> <strong>".$t[423].":</strong> </td>";
			echo "<td class='tbl2' style='padding-left: 15px;'> <a href='".$redir['video'].$data['video_id']."'>".$data['video_name']."</a></td>";
			echo "</tr>";	
			echo "<tr>";
			echo "<td class='tbl1' style='width: 50%; text-align: right; padding-right:5px;'> <strong>".$t[424].":</strong> </td>";
			echo "<td class='tbl2' style='padding-left: 15px;'> <a href='".BASEDIR."profile.php?lookup=".$data['video_user']."'>".$data['creator']."</a> d�a ".strftime("%d.%m.%Y o %H:%M", $data['video_date'])."</td>";
			echo "</tr>";	
			echo "<tr>";
			echo "<td class='tbl1' style='width: 50%; text-align: right; padding-right:5px;'> <strong>".$t[425].":</strong> </td>";
			echo "<td class='tbl2' style='padding-left: 15px;'>".$data['report_reason']."</td>";
			echo "</tr>";	
			echo "<tr>";
			echo "<td class='tbl1' style='width: 50%; text-align: right; padding-right:5px;'> <strong>".$t[426].":</strong> </td>";
			echo "<td class='tbl2' style='padding-left: 15px;'> ".$data['report_description']." </td>";
			echo "</tr>";	
			echo "<tr>";
			echo "<td class='tbl1' style='width: 50%; text-align: right; padding-right:5px;'> <strong>".$t[427].":</strong> </td>";
			echo "<td class='tbl2' style='padding-left: 15px;'> <a href='".BASEDIR."profile.php?lookup=".$data['report_user']."'>".$data['reporter']."</a> d�a ".strftime("%d.%m.%Y o %H:%M", $data['report_date'])." </td>";
			echo "</tr>";	
			echo "<tr>";
			echo "<td class='tbl1' style='width: 50%; text-align: right; padding-right:5px;'> <strong>".$t[428].":</strong> </td>";
			echo "<td class='tbl2' style='padding-left: 15px;'> 
						<a href='".$redir['video'].$data['video_id']."'>".$t[429]."</a> <br/>
						<a href='".$redir['admin']."bans&amp;add=".$data['video_user']."'>".$t[430]." ".$data['creator']."</a> <br/>
						<a href='".$redir['admin']."reports&amp;block=".$data['report_id']."'>".$t[431]."</a> <br/>
						<a href='".$redir['admin']."videos&amp;edit=".$data['video_id']."'>".$t[432]."</a> <br/>
						<a href='".$redir['admin']."reports&amp;delete=".$data['report_id']."'>".$t[433]."</a> <br/>		
					</td>";
			echo "</tr>";				
		echo "</table>";
		closetable();
		
	// Block video
	}elseif (isset($_GET['block']) && isnum($_GET['block'])) {
	
		if (!isset($_GET['block']) OR !isnum($_GET['block'])) redirect($redir['admin']."reports"); 
		$sql = dbquery("SELECT * FROM ".CIN_REPORT." WHERE report_id='".$_GET['block']."'");
		if (!dbrows($sql)) redirect($redir['admin']."reports"); 
		$data = dbarray($sql);
		dbquery("DELETE FROM ".CIN_REPORT." WHERE report_id='".$_GET['block']."'");
		redirect($redir['admin']."videos&amp;block=".$data['report_video']);
			
	// Delete report
	}elseif (isset($_GET['delete']) && isnum($_GET['delete'])) {
		
		dbquery("DELETE FROM ".CIN_REPORT." WHERE report_id='".$_GET['delete']."'");
		redirect($redir['admin']."reports&amp;done_delete");

	} else {
		opentable($cinema['set_name']." - ".$t[422]);	
			echo "<table border='0' align='center' width='100%' cellpadding='0' cellspacing='1'>";
				$sql = dbquery("SELECT t1.*,t2.user_name, t3.* FROM ".CIN_REPORT." as t1 LEFT JOIN ".DB_PREFIX."users as t2 ON t2.user_id=t1.report_user LEFT JOIN ".CIN_VIDEO." as t3 ON t3.video_id=t1.report_video");
				if (!dbrows($sql)) { echo "<tr><td class='tbl1' align='center' colspan='2'> ".$t[434]." </td></tr>"; }
				while ($data = dbarray($sql)) {
					echo "<tr>";
						echo "<td class='tbl1'> ".$data['video_name']." </td>";
						echo "<td class='tbl2'> ".$t[435].": <a href='".BASEDIR."profile.php?lookup=".$data['video_user']."'>".$data['user_name']."</a>  </td>";
						echo "<td class='tbl1' align='center'> ".strftime("%d.%m.%Y ".$t[436]." %H:%M", $data['report_date'])."  </td>";
						echo "<td class='tbl2' align='center'> ".$t[437].": ".$data['report_reason']." </td>";
						echo "<td class='tbl1' align='center'> <a href='".$redir['admin']."reports&amp;work=".$data['report_id']."'>".$t[438]."</a> </td>";
					echo "</tr>";		
				}	
			echo "</table>";	
		closetable();
	}
?>