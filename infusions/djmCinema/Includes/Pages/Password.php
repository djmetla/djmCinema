<?php
/*-------------------------------------------------------+
| PHP-Fusion Content Management System
| Copyright (C) 2002 - 2011 Nick Jones
| http://www.php-fusion.co.uk/
+--------------------------------------------------------+
| Filename:	password.php
| Author: Patrik Hoffmann (djmetla)
+--------------------------------------------------------+
| This program is released as free software under the
| Affero GPL license. You can redistribute it and/or
| modify it under the terms of this license which you
| can read by viewing the included agpl.txt or online
| at www.gnu.org/licenses/agpl.html. Removal of this
| copyright header is strictly prohibited without
| written permission from the original author(s).
+--------------------------------------------------------*/
if (!defined("IN_FUSION")) { die('Access Denied'); }
if (!isset($_GET['aid']) || !isnum($_GET['aid'])) { redirect($redir['home']); }

$sql = dbquery("SELECT gallery_id, gallery_name, gallery_user, gallery_password FROM ".CIN_GALLERY." WHERE gallery_id='".$_GET['aid']."' ");
$data = dbarray($sql);
if (!dbrows($sql)) redirect($redir['home']); 

if (isset($_POST['submit'])) {
	if (empty($_POST['password'])) redirect($redir['page']."password&amp;aid=".$data['gallery_id']."&amp;error_password");	
	if ($_POST['password'] == $data['gallery_password']) {
		setCookie("djmCinemaAlbum", $_POST['password'], time()+30*60);
		redirect($redir['page']."album&amp;aid=".$data['gallery_id']);
	} else {
		redirect($redir['page']."password&amp;aid=".$data['gallery_id']."&amp;error_password");
	}
}

add_to_title("&nbsp;-&nbsp;".$cinema['set_name']." - ".$t[194]);
opentable($cinema['set_name']." - ".$t[195] ." ".$data['gallery_name']);
echo "<div style='padding: 20px;'>";
	echo "<form action='' method='post'>";
		echo "<div style='text-align:center; margin-bottom: 10px;'>".$t[196]." <u>".$data['gallery_name']."</u> </div>";
		echo "<div style='text-align:center;'><input type='password' name='password' /> <input type='submit' value='".$t[197]."' name='submit' class='button' /></div>'";
	echo "</form>";	
echo "</div>";	
closetable(); 
?>