<?php
/*-------------------------------------------------------+
| PHP-Fusion Content Management System
| Copyright (C) 2002 - 2011 Nick Jones
| http://www.php-fusion.co.uk/
+--------------------------------------------------------+
| Filename: tags.php
| Author: Patrik Hoffmann (djmetla)
+--------------------------------------------------------+
| This program is released as free software under the
| Affero GPL license. You can redistribute it and/or
| modify it under the terms of this license which you
| can read by viewing the included agpl.txt or online
| at www.gnu.org/licenses/agpl.html. Removal of this
| copyright header is strictly prohibited without
| written permission from the original author(s).
+--------------------------------------------------------*/
if (!defined("IN_FUSION")) { die('Access Denied'); }
if (!isset($_GET['tid']) || !isnum($_GET['tid'])) { redirect(BASEDIR."index.php"); }

$sql = dbquery("SELECT * FROM ".CIN_TAGS_VALUES." WHERE tag_id='".$_GET['tid']."'");
$tag = dbarray($sql);

if (!dbrows($sql)) redirect($redir['home']);

add_to_title("&nbsp;-&nbsp;".$cinema['set_name']." - ".$t[227].": ".$tag['tag_name']);
opentable($cinema['set_name']." - ".$t[227].": ".$tag['tag_name']);
	echo "<table width='100%' cellpadding='0' cellspacing='1' />";
		echo "<tr> <td class='tbl' width='35%'>".$t[228].":</td> <td class='tbl2'>".$tag['tag_name']."</td>";
		echo "<tr> <td class='tbl'>".$t[231].":</td> <td class='tbl2'>".($tag['tag_age_confirm'] == 1 ? $t[229]:$t[230])."</td>";
		echo "<tr> <td class='tbl'>".$t[232].":</td> <td class='tbl2'>".dbcount("(*)", CIN_TAGS, "tag_tag='".$tag['tag_id']."'")."</td>";
		echo "<tr> <td class='tbl'>".$t[233].":</td> <td class='tbl2'>".$tag['tag_description']."</td>";
	echo "</table>";	
closetable(); 
opentable($tag['tag_name']." ".$t[234]);
 $sql2 = dbquery("SELECT t1.*, t2.* FROM ".CIN_TAGS." as t1 LEFT JOIN ".CIN_VIDEO." as t2 ON t2.video_id=t1.tag_video WHERE tag_tag='".$tag['tag_id']."'");
 echo djmCinemaVideo($sql2, $cinema['set_video_list']);
closetable();
?>