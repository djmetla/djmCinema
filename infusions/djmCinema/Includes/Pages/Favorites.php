<?php
/*-------------------------------------------------------+
| PHP-Fusion Content Management System
| Copyright (C) 2002 - 2011 Nick Jones
| http://www.php-fusion.co.uk/
+--------------------------------------------------------+
| Filename: favorites.php
| Author: Patrik Hoffmann (djmetla)
+--------------------------------------------------------+
| This program is released as free software under the
| Affero GPL license. You can redistribute it and/or
| modify it under the terms of this license which you
| can read by viewing the included agpl.txt or online
| at www.gnu.org/licenses/agpl.html. Removal of this
| copyright header is strictly prohibited without
| written permission from the original author(s).
+--------------------------------------------------------*/
if (!defined("IN_FUSION")) { die('Access Denied'); }
if (!iMEMBER) { redirect($redir['error']."5"); }

if (isset($_GET['delete']) && isnum($_GET['delete'])) {
	
	$control = dbcount("(*)", CIN_FAVORITE, "favorite_id='".$_GET['delete']."' AND favorite_user='".$userdata['user_id']."'");
	if ($control) {
		dbquery("DELETE FROM ".CIN_FAVORITE." WHERE favorite_id='".$_GET['delete']."'");
		redirect($redir['page']."favorites&amp;delete_done");
	} else {	
		redirect($redir['page']."favorites");
    }
}


add_to_title("&nbsp;-&nbsp;".$cinema['set_name']." - ".$t[132].": ".$userdata['user_name']);
opentable($cinema['set_name']." - ".$t[132].": ".$userdata['user_name']);
	
		$sql = dbquery("SELECT t1.*,t2.* FROM ".CIN_FAVORITE." as t1 LEFT JOIN ".CIN_VIDEO." as t2 ON t2.video_id=t1.favorite_video WHERE favorite_user='".$userdata['user_id']."' ORDER BY favorite_date DESC");
		echo "<table width='100%' cellpadding='0' cellspacing='1'>";
		while ($data = dbarray($sql)) {
			echo "<tr>";
				echo "<td class='tbl1'> <a href='".$redir['video'].$data['favorite_video']."'>".$data['video_name']."</a></td>";
				echo "<td class='tbl2' align='center'> ".number_format($data['video_viewed'])."x ".$t[133]." </td>";
				echo "<td class='tbl1' align='center'> ".$t[134].": ".strftime("%d.%m.%Y", $data['favorite_date'])." </td>";
				echo "<td class='tbl2' align='center'> <a href='".$redir['page']."favorites&amp;delete=".$data['favorite_id']."'>".$t[135]."</a> </td>";

			echo "</tr>";
		}	
		echo "</table>";
		if (!dbrows($sql)) echo "<div style='text-align: center; padding: 25px;'>".$t[136]."</div>";
closetable();
?>