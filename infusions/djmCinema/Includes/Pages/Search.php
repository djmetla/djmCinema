<?php
/*-------------------------------------------------------+
| PHP-Fusion Content Management System
| Copyright (C) 2002 - 2011 Nick Jones
| http://www.php-fusion.co.uk/
+--------------------------------------------------------+
| Filename: albums.php
| Author: Patrik Hoffmann (djmetla)
+--------------------------------------------------------+
| This program is released as free software under the
| Affero GPL license. You can redistribute it and/or
| modify it under the terms of this license which you
| can read by viewing the included agpl.txt or online
| at www.gnu.org/licenses/agpl.html. Removal of this
| copyright header is strictly prohibited without
| written permission from the original author(s).
+--------------------------------------------------------*/
if (!defined("IN_FUSION")) { die('Access Denied'); }
add_to_title("&nbsp;-&nbsp;".$cinema['set_name']." - ".$t[217]);
if (!isset($_GET['q']) || empty($_GET['q'])) redirect($redir['home']);

 $_GET['q'] = strip_tags($_GET['q']);

opentable($cinema['set_name']." - ".$t[217]);
	echo "<table border='0' align='center' width='100%' cellpadding='0' cellspacing='0'>";
		echo "<tr> <td colspan='2' class='forum-caption'> ".THEME_BULLET." ".$t[218].": </td></tr>";
		echo "<tr> <td colspan='2' class='tbl'> ".$_GET['q']." </td></tr>";
		
		// Videos
		echo "<tr> <td colspan='2' class='forum-caption'> ".THEME_BULLET." ".$t[219].": </td></tr>";
				$sql = dbquery("SELECT * FROM ".CIN_VIDEO." WHERE video_id='".$_GET['q']."' OR video_name LIKE '%".$_GET['q']."%' OR video_source LIKE '%".$_GET['q']."%' ");
				if (dbrows($sql)) {
					echo "<td colspan='2'>";
					echo "<table border='0' align='center' width='100%' cellpadding='0' cellspacing='1'>";
					while ($data = dbarray($sql)) {
						echo "<tr><td class='tbl' width='50%'> <a href='".$redir['video'].$data['video_id']."'>".$data['video_name']."</a></td>";
						echo "<td class='tbl2' align='center' width='50%'>".ucfirst(strtolower($data['video_source'].".com"))."</td>";
					}
					echo "</table></td>";
				}else{
					echo "<td colspan='2' align='center' style='padding: 15px;'>".$t[220]."</td>";
				}
				unset($sql);		

		// Albums		
		echo "<tr> <td colspan='2' class='forum-caption'> ".THEME_BULLET." ".$t[221].": </td></tr>";
		echo "<tr>";
				$sql = dbquery("SELECT * FROM ".CIN_GALLERY." WHERE gallery_id='".$_GET['q']."' OR gallery_name LIKE '%".$_GET['q']."%' ");
				if (dbrows($sql)) {
					echo "<td colspan='2'>";
					echo "<table border='0' align='center' width='100%' cellpadding='0' cellspacing='1'>";
					while ($data = dbarray($sql)) {
						echo "<tr><td class='tbl' width='50%'> ".($data['gallery_password'] != "" ? "<img src='".INFUSIONS."djmCinema/Images/Icon/locked.png' width='12' height='12' alt='Lock' title='".$t[222]."' />":"")." <a href='".$redir['album'].$data['gallery_id']."'>".$data['gallery_name']."</a></td>";
						echo "<td class='tbl2' align='center' width='50%'>".($data['gallery_type'] == 'USER' ? $t[223]:$t[224])."</td>";
					}
					echo "</table></td>";
				}else{
					echo "<td colspan='2' align='center' style='padding: 15px;'>".$t[220]."</td>";
				}
				unset($sql);
		echo "</tr>";		
		
		// ALbum user
		echo "<tr> <td colspan='2' class='forum-caption'> ".THEME_BULLET." ".$t[225].": </td></tr>";
				$sql = dbquery("SELECT t1.*,t2.* FROM ".DB_PREFIX."users as t1 INNER JOIN ".CIN_GALLERY." as t2 ON t2.gallery_user=t1.user_id WHERE user_id='".$_GET['q']."' OR user_name LIKE '%".$_GET['q']."%' ");
				if (dbrows($sql)) {
					echo "<td colspan='2'>";
					echo "<table border='0' align='center' width='100%' cellpadding='0' cellspacing='1'>";
					while ($data = dbarray($sql)) {
						echo "<tr><td class='tbl' width='50%'> ".($data['gallery_password'] != "" ? "<img src='".INFUSIONS."djmCinema/Images/Icon/locked.png' width='12' height='12' alt='Lock' title='".$t[222]."' />":"")." <a href='".$redir['album'].$data['gallery_id']."'>".$data['gallery_name']."</a></td>";
						echo "<td class='tbl2' align='center' width='25%'> <a href='".BASEDIR."profile.php?lookup=".$data['user_id']."'>".$data['user_name']."</a> </td>";
						echo "<td class='tbl1' align='center' width='25%'>".($data['gallery_type'] == 'USER' ? $t[223]:$t[224])."</td>";
					}
					echo "</table></td>";
				}else{
					echo "<td colspan='2' align='center' style='padding: 15px;'>".$t[220]."</td>";
				}
				unset($sql);	

		// Video user		
		echo "<tr> <td colspan='2' class='forum-caption'> ".THEME_BULLET." ".$t[226].": </td></tr>";
				$sql = dbquery("SELECT t1.*,t2.* FROM ".DB_PREFIX."users as t1 INNER JOIN ".CIN_VIDEO." as t2 ON t2.video_user=t1.user_id WHERE user_id='".$_GET['q']."' OR user_name LIKE '%".$_GET['q']."%' ");
				if (dbrows($sql)) {
					echo "<td colspan='2'>";
					echo "<table border='0' align='center' width='100%' cellpadding='0' cellspacing='1'>";
					while ($data = dbarray($sql)) {
						echo "<tr><td class='tbl' width='50%'> <a href='".$redir['video'].$data['video_id']."'>".$data['video_name']."</a></td>";
						echo "<td class='tbl2' align='center' width='25%'> <a href='".BASEDIR."profile.php?lookup=".$data['user_id']."'>".$data['user_name']."</a> </td>";
						echo "<td class='tbl1' align='center' width='25%'>".ucfirst(strtolower($data['video_source'].".com"))."</td>";
					}
					echo "</table></td>";
				}else{
					echo "<td colspan='2' align='center' style='padding: 15px;'>".$t[220]."</td>";
				}
				unset($sql);		
	echo "</table>";
closetable();
?>