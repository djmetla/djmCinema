<?php
/*-------------------------------------------------------+
| PHP-Fusion Content Management System
| Copyright (C) 2002 - 2011 Nick Jones
| http://www.php-fusion.co.uk/
+--------------------------------------------------------+
| Filename: album.php
| Author: Patrik Hoffmann (djmetla)
+--------------------------------------------------------+
| This program is released as free software under the
| Affero GPL license. You can redistribute it and/or
| modify it under the terms of this license which you
| can read by viewing the included agpl.txt or online
| at www.gnu.org/licenses/agpl.html. Removal of this
| copyright header is strictly prohibited without
| written permission from the original author(s).
+--------------------------------------------------------*/
if (!defined("IN_FUSION")) { die('Access Denied'); }
add_to_title("&nbsp;-&nbsp;".$cinema['set_name']." - ".$t[70]);
if (!iMEMBER) { redirect($redir['error']."5"); }  


if (isset($_POST['add_new_video'])) {

	 $form = array(
		'name' 			=> (!empty($_POST['video_name']) ? htmlspecialchars($_POST['video_name']):"Bez n�zvu"),
		'url'				=> strip_tags($_POST['video_url']),
		'album'			=> ($_POST['video_album'] == 'no_album' ? NULL:$_POST['video_album']),
		'comments'		=> (isset($_POST['video_comments']) && $cinema['set_allow_comments'] == 1 ? 1:0),
		'ratings'			=> (isset($_POST['video_ratings']) && $cinema['set_allow_ratings'] == 1 ? 1:0),
		'access'			=> ($_POST['video_access'] == 1 ? 1:0),
		'description'		=> htmlspecialchars($_POST['video_description']),
		'accept'			=> ($cinema['set_allow_admin_accept'] ? 0:1),
		'user'				=> $userdata['user_id'],
		'time'				=> time()
	 );

	 
	 $make = MovieMaker($form['url']);
	 if ($make == "errno1") { 	redirect($redir['error']."6"); 
	 } elseif ($make == "errno2") { redirect($redir['error']."7"); 
	 } elseif ($make == "errno3") { redirect($redir['error']."8"); 
	 } else {
	 
	 $video 				= explode("|", $make);
	 $video['code']		= $video[0];
	 $video['portal']	= $video[1];
	 $video['image']	= $video[2];
	 $db_portal			= $cinema['set_allow_'.$video['portal']];
	     

		// Check VideoServer access
		if (!$db_portal) { redirect($redir['error']."10"); }
		
		// Check Album access
		if ($form['album'] != NULL) {
			if (!iADMIN) {
				$album_true = dbcount("(*)", CIN_GALLERY, "gallery_id='".$form['album']."' AND gallery_user='".$form['user']."' AND gallery_type='USER'");
				if (!$album_true) { redirect($redir['error']."9"); } 	
			}	
		}	
		 
		// Allow Accept
		if (iADMIN) { $accept = "1"; } else { $accept = $form['accept']; }
		 
		// Insert video 
		dbquery("INSERT INTO ".CIN_VIDEO." (video_name, video_code, video_thumbnail, video_gallery, video_source, video_status, video_access, video_date, video_user, video_description, video_rating, video_comment, video_viewed) VALUES
															 ('".$form['name']."','".$video['code']."','".$video['image']."','".($form['album'] == NULL ? 0:$form['album'])."','".strtoupper($video['portal'])."','".$accept."','".$form['access']."','".$form['time']."','".$form['user']."','".$form['description']."','".$form['ratings']."','".$form['comments']."','0') "); 
		 
		// Tags core 
		$video_sql 		= dbquery("SELECT * FROM ".CIN_VIDEO." WHERE video_user='".$form['user']."' AND video_date='".$form['time']."' AND video_code='".$video['code']."' ");
		$video_data 	= dbarray($video_sql);
		for ($i = 0;$i < count($_POST['video_tag']);$i++) {
			$tag = $_POST['video_tag'][$i];
			$sql = dbquery("SELECT * FROM ".CIN_TAGS_VALUES." WHERE tag_id='".$tag."' ");
			$security = dbcount("(*)", CIN_TAGS, "tag_video='".$video_data['video_id']."' AND tag_tag='".$tag."'");
			if (dbrows($sql) && !$security) {
				dbquery("INSERT INTO ".CIN_TAGS." (tag_video, tag_tag) VALUES ('".$video_data['video_id']."','".$tag."')");
			}
		}
		
	 }
	 $sql = dbquery("SELECT video_id FROM ".CIN_VIDEO." WHERE video_name='".$form['name']."' AND video_user='".$form['user']."' AND video_date='".$form['time']."'");
	 $cache = dbarray($sql);
	 
	 // Admin message
	 $admin_sql = dbquery("SELECT * FROM ".CIN_ADMIN." WHERE admin_message_accept='1' ");
	 while ( $admin_data = dbarray($admin_sql)) {
		$Subject = $t[85];
		$Message = $t[87]." ".$userdata['user_name']." ".$t[86];
		dbquery("INSERT INTO ".DB_PREFIX."messages (message_to, message_from, message_subject, message_message,message_datestamp) VALUES ('".$admin_data['admin_user']."','".$userdata['user_id']."','".$Subject."','".$Message."','".time()."')");
	 }
	 
	 redirect($redir['video'].$cache['video_id']);
	 unset($_POST, $form, $make, $sql, $cache);
}






opentable($cinema['set_name']." - ".$t[70]);
echo "<form action='' method='post'>";
	echo "<table border='0' align='center' width='100%' cellpadding='0' cellspacing='1'>";
		echo "<tr>";
		echo "<td class='tbl1' style='width: 40%; text-align: right; padding-right:5px;'> <strong>* ".$t[71].": </strong> <br><span style='font-size: 9px;'>".$t[72]."</span> </td>";
		echo "<td class='tbl2' style='padding-left: 15px;'> <input type='text' class='textbox' name='video_name' style='width: 200px;' /> </td>";
		echo "</tr>";
		
		echo "<tr>";
		echo "<td class='tbl1' style='width: 40%; text-align: right; padding-right:5px;'> <strong>* ".$t[73].": </strong> <br><span style='font-size: 9px;'>".$t[74]."</span>  </td>";
		echo "<td class='tbl2' style='padding-left: 15px;'> <input type='text' class='textbox' name='video_url' style='width: 250px;' /> <a href='javascript:void(0);' id='djmCinemaOpenHelp' style='font-size: 9px;'>".$t[75]."</a> </td>";
		echo "</tr>";
		
		echo "<tr id='djmCinemaHelp' style='display: none;'>";
		echo "<td class='tbl2' colspan='2' style='font-size: 10px; padding: 10px;'> 
				<strong>".$t[76]."</strong> <br/>
				".($cinema['set_allow_youtube'] ? "<a href='http://www.youtube.com' target='_blank'>Youtube.com</a>&nbsp;":"")."
				".($cinema['set_allow_vimeo'] ? "<a href='http://www.vimeo.com' target='_blank'>Vimeo.com</a>&nbsp;":"")."
				".($cinema['set_allow_dailymotion'] ? "<a href='http://www.dailymotion.com' target='_blank'>Dailymotion.com</a>&nbsp;":"")."
				".($cinema['set_allow_metacafe'] ? "<a href='http://www.metacafe.com' target='_blank'>Metacafe.com</a>&nbsp;":"")."
				".($cinema['set_allow_break'] ? "<a href='http://www.break.com' target='_blank'>Break.com</a>&nbsp;":"")."
				".($cinema['set_allow_redtube'] ? "<a href='http://www.redtube.com' target='_blank'>Redtube.com (18+)</a>&nbsp;":"")."
				".($cinema['set_allow_pornhub'] ? "<a href='http://www.pornhub.com' target='_blank'>Pornhub.com (18+)</a>&nbsp;":"")."
	
				<br/><br/>
				<strong>".$t[77]."</strong> <br/>
				".$t[78]."
				 <u>http://www.youtube.com/watch?v=YXFSVoVqhYw</u>
				
				<br/><br/>
				<strong>".$t[79]."</strong> <br/>
				".($cinema['set_allow_admin_accept'] ? $t[80]:$t[81])."
				 
				<br/><br/><a href='javascript:void(0);' id='djmCinemaCloseHelp' style='float: right;'>".$t[82]."</a>
				</td>";
		echo "</tr>";
		
		echo "<tr>";
		echo "<td class='tbl1' valign='top' style='width: 40%; text-align: right; padding-right:5px;'> <strong> ".$t[83].": </strong> <br><span style='font-size: 9px;'>".$t[84]."</span> </td>";
		echo "<td class='tbl2' style='padding-left: 15px;'>"; 
				if (iADMIN) { $add_sql = "OR gallery_type='PUBLIC'"; }else{ $add_sql = ""; }
				$sql_album = dbquery("SELECT * FROM ".CIN_GALLERY." WHERE gallery_user='".$userdata['user_id']."' AND gallery_type='USER' OR gallery_type='PUBLIC' AND gallery_allow_insert='1' ".$add_sql);
				if (dbrows($sql_album)) {
 					echo "<select name='video_album'>
								<option value='no_album'>".$t[88]."</option>";
								while ($albums = dbarray($sql_album)) {
									echo "<option value='".$albums['gallery_id']."'>".$albums['gallery_name']." (".dbcount("(*)", CIN_VIDEO, "video_gallery='".$albums['gallery_id']."'").") ".($albums['gallery_type'] == 'PUBLIC' ? "[Web]":"")."</option>";
								}	
							echo "</select> ".($cinema['set_allow_private_albums'] ? " ".$t[89]." <a href='".$redir['page']."myalbums&amp;new'>".$t[90]."</a>":"");				
				} else {
					echo "<select name='video_album'> <option value='no_album'>".$t[88]."</option></select> ".($cinema['set_allow_private_albums'] ? " ".$t[89]." <a href='".$redir['page']."myalbums&amp;new'>".$t[91]."</a>":"")." ";
				}
	
		echo "</td>";
		echo "</tr>";			

		echo "<tr>";
		echo "<td class='tbl1' style='width: 40%; text-align: right; padding-right:5px;'> <strong> ".$t[92].": </strong> <br><span style='font-size: 9px;'>".$t[93]."</span> </td>";
		echo "<td class='tbl2' style='padding-left: 15px;'> <input type='checkbox' name='video_comments' ".($cinema['set_allow_comments'] == 1 ? "checked":"disabled")."/> <i> ".($cinema['set_allow_comments'] == 0 ? $t[94]:"")."</i> </td>";
		echo "</tr>";

		echo "<tr>";
		echo "<td class='tbl1' style='width: 40%; text-align: right; padding-right:5px;'> <strong> ".$t[95].": </strong> <br><span style='font-size: 9px;'>".$t[96]."</span> </td>";
		echo "<td class='tbl2' style='padding-left: 15px;'> <input type='checkbox' name='video_ratings' ".($cinema['set_allow_ratings'] == 1 ? "checked":"disabled")."/> ".($cinema['set_allow_ratings'] == 0 ? "(<i>".$t[97]."</i>)":"")." </td>";
		echo "</tr>";		
		
		echo "<tr>";
		echo "<td class='tbl1' style='width: 40%; text-align: right; padding-right:5px;'> <strong>* ".$t[98].": </strong> <br><span style='font-size: 9px;'>".$t[99]."</span> </td>";
		echo "<td class='tbl2' style='padding-left: 15px;'> <input type='radio' name='video_access' value='0' checked/> ".$t[100]." <input type='radio' name='video_access' value='1' /> ".$t[101]."</td>";
		echo "</tr>";
		
		echo "<tr>";
		echo "<td class='tbl1' valign='top' style='width: 40%; text-align: right; padding-right:5px;'> <strong> ".$t[102].": </strong> <br><span style='font-size: 9px;'>".$t[103]."</span> </td>";
		echo "<td class='tbl2' style='padding-left: 15px;'>";
				
				$sql_tags = dbquery("SELECT * FROM ".CIN_TAGS_VALUES);
				if (dbrows($sql_tags)) {
					while ($tags = dbarray($sql_tags)) {
						echo "<input type='checkbox' name='video_tag[]' value='".$tags['tag_id']."'/> <span style='position:absolute; margin-top: 3px;'>".$tags['tag_name']."</span> <br/>";
					}
				}else{
					echo "<i>".$t[104]."</i>";
				}
		echo "</td>";
		echo "</tr>";	
		
		echo "<tr>";
		echo "<td class='tbl1' valign='top' style='width: 50%; text-align: right; padding-right:5px;'> <strong>".$t[105].":</strong> <br><span style='font-size: 9px;'> ".$t[106]." </span> </td>";
		echo "<td class='tbl2' style='padding-left: 15px;'> <textarea name='video_description' style=' width: 350px; height: 150px;'></textarea> </td>";
		echo "</tr>";		

		echo "<tr><td colspan='2' style='padding: 10px;'><input type='submit' value='".($cinema['set_allow_admin_accept'] ? $t[107]:$t[108])."' name='add_new_video' class='button'> ".(iADMIN ? $t[109]:"")." </td></tr>";	
			
	echo "</table>";
echo "</form>";	
closetable();
?>