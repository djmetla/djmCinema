<?php
/*-------------------------------------------------------+
| PHP-Fusion Content Management System
| Copyright (C) 2002 - 2011 Nick Jones
| http://www.php-fusion.co.uk/
+--------------------------------------------------------+
| Filename: albums.php
| Author: Patrik Hoffmann (djmetla)
+--------------------------------------------------------+
| This program is released as free software under the
| Affero GPL license. You can redistribute it and/or
| modify it under the terms of this license which you
| can read by viewing the included agpl.txt or online
| at www.gnu.org/licenses/agpl.html. Removal of this
| copyright header is strictly prohibited without
| written permission from the original author(s).
+--------------------------------------------------------*/
if (!defined("IN_FUSION")) { die('Access Denied'); }
add_to_title("&nbsp;-&nbsp;".$cinema['set_name']." - ".$t[129]);


opentable($cinema['set_name']." - ".$t[129]);
	$sql = dbquery("SELECT * FROM ".CIN_GALLERY." WHERE gallery_type='USER' ");
	echo djmCinemaAlbum($sql, $cinema['set_album_list']);
	unset($sql);
closetable();
?>