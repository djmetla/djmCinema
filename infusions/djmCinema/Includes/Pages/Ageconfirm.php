<?php
/*-------------------------------------------------------+
| PHP-Fusion Content Management System
| Copyright (C) 2002 - 2011 Nick Jones
| http://www.php-fusion.co.uk/
+--------------------------------------------------------+
| Filename:	ageconfirm.php
| Author: Patrik Hoffmann (djmetla)
+--------------------------------------------------------+
| This program is released as free software under the
| Affero GPL license. You can redistribute it and/or
| modify it under the terms of this license which you
| can read by viewing the included agpl.txt or online
| at www.gnu.org/licenses/agpl.html. Removal of this
| copyright header is strictly prohibited without
| written permission from the original author(s).
+--------------------------------------------------------*/
if (!defined("IN_FUSION")) { die('Access Denied'); }
if (!isset($_GET['vid']) || !isnum($_GET['vid'])) { redirect($redir['home']); }
if (isset($_COOKIE['djmCinemaAge'])) redirect($redir['home']); 

if (isset($_GET['accept'])) {
	 $time = time() + 31 * (24 * 60 * 60);
	 setCookie("djmCinemaAge", 1, $time);
	 redirect($redir['video'].$_GET['vid']);
}

$age_text = $t[110];
add_to_title("&nbsp;-&nbsp;".$cinema['set_name']." - ".$t[111]);
opentable($cinema['set_name']." - ".$t[111]);
echo "<div style='padding: 20px; text-align: center;'>";
	echo $age_text."<br/><br/>";
	
	echo "<a href='".$redir['page']."ageconfirm&amp;accept&amp;vid=".$_GET['vid']."'>".$t[112]."</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
	echo "<a href='".$redir['home']."'>".$t[113] ."</a>";
	
echo "</div>";	
closetable(); 
?>

