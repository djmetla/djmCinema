$(function() {

 $('#djmCinemaOpenHelp').click(function(){
	$('#djmCinemaHelp').show();
	$(this).hide();
	return false;
 });
 
 $('#djmCinemaCloseHelp').click(function(){
	$('#djmCinemaHelp').hide();
	$('#djmCinemaOpenHelp').show();
	return false;
 });
 
 $('#djmCinemaCloseTooltipMessage').click(function(){
	$('#djmCinemaTooltipMessage').hide();
	return false;
 });
 
 $('.djmCinClose').click(function(){
	$('.djmCinemaAClose').slideUp();
	return false;
 }); 

 $('#djmCinemaShowMore').click(function(){
	$('#djmCinemaMore').css('height','100%');
	$(this).parent().hide();
	$('#toolHideMore').show();
	return false;
 });
 
  $('#djmCinemaHideMore').click(function(){
	$('#djmCinemaMore').css('height','150px');
	$(this).parent().hide();
	$('#toolShowMore').show();
	return false;
 });
 
 $('#copy_link').click(function(){
	$(this).select();
	return false;
 });
 
 $('#ShareWindowClose').click(function(){
	$('#Share').slideUp();
	return false;
 });
 
 $('#djmCinemaShare').click(function() {
	$('#Share').slideDown();
	return false;
 });
  
 $('.commentTooltipClose').click(function(){
	$('#commentTooltip').hide();
	return false;
 }); 
 
 $('#cinemaOrder').change(function(){
	var item = $(this).val(),
	      url = 	$(this).data('value');
	location.href = url + item;
 });
 

 
 
 $('#cinemaSearch').ready(function() {
   
    var defined = $('#cinemaSearch').data('defined');
	$('#cinemaSearch').val(defined);
	
		$('#cinemaSearch').mouseout(function(){
			if ($('#cinemaSearch').val()=='') {
				$('#cinemaSearch').val(defined);
				$('#cinemaSearchSubmit').hide();
			}
		});
	
		$('#cinemaSearch').mouseenter(function(){
		    if ($('#cinemaSearch').val()==defined) {
				$(this).val('');
			}
			
			if ($('#cinemaSearch').val()!=defined) {
				$('#cinemaSearchSubmit').show();
			}				
			
		});
	

 
 });
 
 
 // AJAX
 $('#djmCinemaLike').click(function(){
			var value = $(this).data('value');
			$.get('./Includes/Libs/djmCinema.ajax.php', { type: 'like', data: value }, function(data) {
			
				if (data=='done') {
					$('.djmCinemaAClose').slideUp();
					$('#DoneLike').slideDown();
					$('#djmCinemaLike').hide();
					$('#djmCinemaDislike').hide();
				}else{
					$('.djmCinemaAClose').slideUp();
					$('#ExistLike').slideDown();
					$('#djmCinemaLike').hide();
					$('#djmCinemaDislike').hide();
				}	
				
			});
 });
 
 $('#djmCinemaDislike').click(function(){
			var value = $(this).data('value');
			$.get('./Includes/Libs/djmCinema.ajax.php', { type: 'dislike', data: value }, function(data) {
			
				if (data=='done') {
					$('.djmCinemaAClose').slideUp();
					$('#DoneLike').slideDown();
					$('#djmCinemaLike').hide();
					$('#djmCinemaDislike').hide();
				}else{
					$('.djmCinemaAClose').slideUp();
					$('#ExistLike').slideDown();
					$('#djmCinemaLike').hide();
					$('#djmCinemaDislike').hide();
				}	
				
			});
 });

 $('#djmCinemaFavorite').click(function(){
			var value = $(this).data('value');
			$.get('./Includes/Libs/djmCinema.ajax.php', { type: 'favorite', data: value }, function(data) {
		
				if (data=='done') {
					$('.djmCinemaAClose').slideUp();
					$('#DoneFavorite').slideDown();
				}else{
					$('.djmCinemaAClose').slideUp();
					$('#ExistFavorite').slideDown();
				}			
				
			});
 }); 
 
  $('#djmCinemaUnFavorite').click(function(){
			var value = $(this).data('value');
			$.get('./Includes/Libs/djmCinema.ajax.php', { type: 'unfavorite', data: value }, function(data) {
					
					$('.djmCinemaAClose').slideUp();
					$('#DoneUnFavorite').slideDown();

			});
 }); 
 
 $('#djmCinemaFormComment').keyup(function(){
			var max = parseInt($(this).attr('maxlength'));
			if($(this).val().length > max){ $(this).val($(this).val().substr(0, $(this).attr('maxlength'))); }
			$('.charsRemaining').html((max - $(this).val().length));	
			$('.showCharsRemaining').show();
			return false;
 });
 

 $('#postComment').click(function(){
	$.ajax({
		type: 'POST',
		url: './Includes/Libs/djmCinema.addcomments.ajax.php',
		contentType: "application/x-www-form-urlencoded;charset=UTF-8",
		data: $('#CommentForm').serialize(),
		success: function(data) {
				
			$('#djmCinemaFormComment').val('');
			$.ajax({
				type: "GET",
				url: "./Includes/Libs/djmCinema.comments.js",
				dataType: "script"
			});
		
			if (data=='error') {
				$('#commentTooltip').children().hide();
				$('#commentTooltip').show();
				$('#commentError').show();
			}else if(data=='warning') {
				$('#commentTooltip').children().hide();
				$('#commentTooltip').show();
				$('#commentWarning').show();			
			}else if(data=='done') {
				$('#DiscussionEmpty').hide();
				$('#commentTooltip').children().hide();
				$('#commentTooltip').show();
				$('#commentSuccess').show();			
			}
		},
		error : function() {
				$('#commentTooltip').children().hide();
				$('#commentTooltip').show();
				$('#commentError').show();
		} 
	});
	return false;
 });
});