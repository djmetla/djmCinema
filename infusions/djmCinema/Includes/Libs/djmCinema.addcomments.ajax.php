<?php
require "../../../../maincore.php";
if (!defined("IN_FUSION")) { die('error'); }
if (!iMEMBER) { die('error'); }
if (empty($_POST['parent']) || !isnum($_POST['parent'])) { die('error'); }
if (empty($_POST['comment']) || !isset($_POST['comment'])) { die('warning'); }



// Data
$id = $_POST['parent'];
$comment = htmlspecialchars($_POST['comment']);

// Check video && Comment Access
$video = dbcount("(*)", DB_PREFIX."cinema_video", "video_id='".$id."' AND video_comment='1'");

if ($video) {

	$form = array(
		'video' => $id,
		'comment' => $comment,
		'time' => time(),
		'user' => $userdata['user_id']		
	);
	dbquery("INSERT INTO ".DB_PREFIX."cinema_comments (comment_item, comment_value, comment_user, comment_date) VALUES ('".$form['video']."', '".$form['comment']."','".$form['user']."','".$form['time']."')");
	die('done');

} else { die('error'); } 
?>
