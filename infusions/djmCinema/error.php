<?php
/*-------------------------------------------------------+
| PHP-Fusion Content Management System
| Copyright (C) 2002 - 2011 Nick Jones
| http://www.php-fusion.co.uk/
+--------------------------------------------------------+
| Filename: error.php
| Author: Patrik Hoffmann (djmetla)
+--------------------------------------------------------+
| This program is released as free software under the
| Affero GPL license. You can redistribute it and/or
| modify it under the terms of this license which you
| can read by viewing the included agpl.txt or online
| at www.gnu.org/licenses/agpl.html. Removal of this
| copyright header is strictly prohibited without
| written permission from the original author(s).
+--------------------------------------------------------*/
require "../../maincore.php";
require_once THEMES."templates/header.php";
require INFUSIONS."djmCinema/Includes/Core.php";
if (!defined("IN_FUSION")) { die('Access Denied'); }
	$available = array(1,2,3,4,5,6,7,8,9,10,11,12);

	$error = array(
		1 => $t[22],
		2 => $t[23],
		3 => $t[24] ,
		4 => $t[25],
		5 => $t[26],
		6 => $t[27],
		7 => $t[28],
		8 => $t[29],
		9 => $t[30],
		10 => $t[31],
		11 => $t[32],
		12 => $t[33],
		);

	$des = array(
		1 => $cinema['set_name']." ".$t[34],
		2 => $t[35],
		3 => $t[36],
		4 => $t[37],
		5 => $t[38],
		6 => $t[39],
		7 => $t[40],
		8 => $t[41],
		9 => $t[42],
		10 => $t[43],
		11 => $t[44],
		12 => $t[45]
		);

		
	
if(!isset($_GET['id']) || !isnum($_GET['id']) || !in_array($_GET['id'], $available) ) { redirect(BASEDIR."index.php"); }	
$id = $_GET['id'];
$title = $error[$id];
$desc = $des[$id];

// Display
add_to_title("&nbsp;-&nbsp;".$cinema['set_name']." - ".$title);
opentable($cinema['set_name']." - ".$title);
echo "<div style='text-align: center; padding:25px;'>".$desc." <br/><br/> <a href='".$redir['home']."'>".$t[46]."</a> </div>";
closetable();

echo DjmCopyright();
require_once THEMES."templates/footer.php";
?>