<?php
/*-------------------------------------------------------+
| PHP-Fusion Content Management System
| Copyright (C) 2002 - 2011 Nick Jones
| http://www.php-fusion.co.uk/
+--------------------------------------------------------+
| Filename: ban.php
| Author: Patrik Hoffmann (djmetla)
+--------------------------------------------------------+
| This program is released as free software under the
| Affero GPL license. You can redistribute it and/or
| modify it under the terms of this license which you
| can read by viewing the included agpl.txt or online
| at www.gnu.org/licenses/agpl.html. Removal of this
| copyright header is strictly prohibited without
| written permission from the original author(s).
+--------------------------------------------------------*/
require "../../maincore.php";
require_once THEMES."templates/header.php";
require INFUSIONS."djmCinema/Includes/Core.php";
if (!iMEMBER) { redirect(BASEDIR."index.php"); }
if (!isset($_GET['id']) || !isnum($_GET['id'])) { redirect(BASEDIR."index.php"); }

$sql = dbquery("SELECT * FROM ".CIN_BAN." WHERE ban_id='".$_GET['id']."'");
if (!dbrows($sql)) redirect($redir['home']);

$data = dbarray($sql);
$expiration = $data['ban_date'] + $data['ban_expiration'];
if ($data['ban_expiration'] != 0 && $expiration < time()) redirect($redir['home']);

	if ($data['ban_expiration'] != 0) {
		$end = $t[47]." ".strftime("%d.%m.%Y o %H:%M", $expiration);
	} else {
		$end = $t[48];
	}	

	opentable($t[49]);
		echo "<div style='padding: 15px;'>";
		echo "<center> <span style='font-size:16px; font-weight: bold;'>".$t[50]."</span> <br/><br/> ".$data['ban_reason']." <br/><br/> ".($data['ban_expiration'] != 0 ? "<span style='font-size: 10px;'>".$end."</span>":"")." </center>";
		echo "</div>";
	closetable();
require_once THEMES."templates/footer.php";
?>